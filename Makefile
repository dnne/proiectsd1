.PHONY: build clean

CFLAGS=-Wall -g -Wextra -std=c++11
EXE=main

build: $(EXE)

build/generic_data_structures/singly_linked_list.o: src/generic_data_structures/singly_linked_list.cpp src/generic_data_structures/singly_linked_list.h src/Clase/Produs.h
	g++ -c $(CFLAGS) $< -o $@

build/generic_data_structures/resizable.o: src/generic_data_structures/resizable.cpp src/generic_data_structures/resizable.h
	g++ -c $(CFLAGS) $< -o $@

build/Clase/Magazine.o: src/Clase/Magazine.cpp src/Clase/Magazine.h src/generic_data_structures/resizable.h src/typedefs.h
	g++ -c $(CFLAGS) $< -o $@

build/Clase/Categorii.o: src/Clase/Categorii.cpp src/Clase/Categorii.h src/generic_data_structures/resizable.h src/typedefs.h
	g++ -c $(CFLAGS) $< -o $@

build/hash_functions.o: src/hash_functions.cpp src/hash_functions.h src/typedefs.h
	g++ -c $(FLAGS) $< -o $@

build/compare_functions.o: src/compare_functions.cpp src/compare_functions.h src/typedefs.h
	g++ -c $(FLAGS) $< -o $@

build/load.o: src/load.cpp src/load.h src/Clase/Categorii.h src/Clase/Produs.h src/generic_data_structures/singly_linked_list.h src/generic_data_structures/resizable.h src/Clase/Tranzactie.h src/Clase/Magazine.h src/generic_data_structures/hash_map.h src/typedefs.h
	g++ -c $(CFLAGS) $< -o $@

build/main.o: src/main.cpp src/load.h src/Clase/Categorii.h src/Clase/Produs.h src/generic_data_structures/singly_linked_list.h src/generic_data_structures/resizable.h src/Clase/Tranzactie.h src/Clase/Magazine.h src/generic_data_structures/hash_map.h src/compare_functions.h src/hash_functions.h src/typedefs.h src/web.h
	g++ -c $(CFLAGS) $< -o $@

build/solve.o: src/solve.cpp src/solve.h src/Clase/Produs.h src/Clase/Tranzactie.h src/generic_data_structures/pair.h src/generic_data_structures/hash_map.h src/generic_data_structures/singly_linked_list.h src/generic_data_structures/triplet.h src/Clase/Categorii.h src/typedefs.h
	g++ -c $(CFLAGS) $< -o $@

build/interactive.o: src/interactive.cpp src/interactive.h src/Clase/Produs.h src/Clase/Tranzactie.h src/generic_data_structures/pair.h src/generic_data_structures/hash_map.h src/generic_data_structures/singly_linked_list.h src/generic_data_structures/triplet.h src/typedefs.h
	g++ -c $(CFLAGS) $< -o $@

build/web.o: src/web.cpp src/web.h src/Clase/Produs.h src/Clase/Tranzactie.h src/generic_data_structures/pair.h src/generic_data_structures/hash_map.h src/generic_data_structures/singly_linked_list.h src/generic_data_structures/triplet.h src/typedefs.h
	g++ -c $(CFLAGS) $< -o $@

$(EXE): build/main.o build/load.o build/Clase/Categorii.o build/generic_data_structures/resizable.o build/generic_data_structures/singly_linked_list.o build/hash_functions.o build/Clase/Magazine.o build/solve.o build/interactive.o build/compare_functions.o build/web.o
	g++ $(CFLAGS) $^ -o $@

clean:
	rm "$(EXE)" build/*/*.o build/*.o