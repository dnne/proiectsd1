#include "hash_functions.h"

#define GRP_64 0x9e37fffffffc0001UL //valoare statica
#define PRIME_PRODUS 53
#define PRIME_BON 5003
#define PRIME_UNIX 5003
#define PRIME_PROD_PAIR 1031

// Functiile de hash
// se aleg numere prime apropiate numarului total de buckets pentru a evita coliziunile

// Functie de hash cu numar buckets egal cu 2 ^ bit
unsigned int func_x64(unsigned long long key, unsigned bit)
{
    return (key * GRP_64) >> (64-bit);
}

// Functia de hash pentru perechile de produse - task 5 director executiv
unsigned int hash_prod_pair(unsigned long long key, unsigned buckets)
{
	return (key * PRIME_PROD_PAIR) % buckets;
}

// Functia de hash pentru id-urile bonurilor
unsigned int hash_id_bon(unsigned long long key, unsigned buckets)
{
	return (key * PRIME_BON) % buckets;
}

// Functia de hash pentru id-urile produselor
unsigned int hash_id_produs(unsigned key, unsigned buckets)
{
	return (key * PRIME_PRODUS) % buckets;
}

// Functia de hash pentru id-urile produselor
unsigned int hash_timp_unix(int key, unsigned buckets)
{
        return (key * PRIME_UNIX) % buckets;
}
