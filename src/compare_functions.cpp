#include "compare_functions.h"

int compare_pair_categ_sales(
	const Pair<IdCategorie,unsigned int>& p1,
	const Pair<IdCategorie,unsigned int>& p2)
{
	if(p1.t2 < p2.t2)
		return 1;

	if(p1.t2 > p2.t2)
		return -1;

	return 0;
}

int compare_triplet_prod_sales(
	const Triplet<IdProdus,IdProdus,unsigned int>& triplet1,
	const Triplet<IdProdus,IdProdus,unsigned int>& triplet2)
{
	if(triplet1.t3 < triplet2.t3)
		return 1;

	if(triplet1.t3 > triplet2.t3)
		return -1;

	return 0;
}

// comparare pentru task-ul 1.2
int compare_pair_prod_sales(
	const Pair<IdProdus,unsigned int>& p1,
	const Pair<IdProdus,unsigned int>& p2)
{
	if(p1.t1 < p2.t1)
		return -1;

	if(p1.t1 > p2.t1)
		return 1;

	return 0;
}


//comparare pentru task-ul 2.2.1
int compare_pair_time_counter(
        const Pair<Time,int>& p1,
        const Pair<Time,int>& p2
                              )
{
        if(p1.t2 < p2.t2)
                return 1;
        if(p1.t2 > p2.t2)
		return -1;
        return 0;
}

//comparare pentru task-ul 2.2.2
int compare_pair_counter_lunazi(
        const Pair<unsigned int,unsigned int>& p1,
        const Pair<unsigned int,unsigned int>& p2
                              )
{
        if(p1.t1 < p2.t1)
                return 1;
        if(p1.t1 > p2.t1)
                return -1;
        return 0;
}
//camparare pentru task-ul 3
int comparare_tranzactii(const Tranzactie& t1, const Tranzactie& t2)
{
       if(t1.get_time() < t2.get_time()){
               return -1;
       }

       return 1;
}

