#include "web.h"

void makeWeb(Categorii& categorii,
	Magazine& magazine,
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii,
	ResizableArray< SinglyLinkedList<IdSlot> >& sloturi_pe_produse,
	ResizableArray< SinglyLinkedList<Triplet <IdPalet, IdProdus, NrItems > > > & paleti_pe_sloturi
	)
{
	std::ofstream out_file(index_file.c_str());
	std::ifstream input_file;
	std::string line;
	if(!out_file) // Eroare deschidere
		return;

	input_file.open(index_input_file1, std::ifstream::in);
	if(!input_file)
	{
		out_file.close();
		return;
	}

	while(getline(input_file, line))
	{
		out_file<<line;
	}
	input_file.close();

	SinglyLinkedList< Triplet<IdMagazin,unsigned int,unsigned long long> > results1_1;
	vanzariMagazine(results1_1, produse, tranzactii);
	SinglyLinkedList< Triplet<IdMagazin,unsigned int,unsigned long long> >::Iterator it1_1;


	for(it1_1 = results1_1.begin() ; it1_1.isValid() ; ++it1_1)
	{
		out_file<<"[";
		out_file<<it1_1.getValue().t1;
		out_file<<",'"<<magazine.getMagazin(it1_1.getValue().t1)<<"'";
		out_file<<","<<it1_1.getValue().t2;
		out_file<<","<<it1_1.getValue().t3;
		out_file<<"],\n";
	}

	input_file.open(index_input_file2, std::ifstream::in);
	if(!input_file)
	{
		out_file.close();
		return;
	}
	while(getline(input_file, line))
	{
		out_file<<line;
	}
	input_file.close();

	SinglyLinkedList< Pair<IdProdus,unsigned int> > results1_2;
	vanzariProduse(results1_2, produse, tranzactii);
	SinglyLinkedList< Pair<IdProdus,unsigned int> >::Iterator it1_2;
	for(it1_2 = results1_2.begin() ; it1_2.isValid() ; ++it1_2)
	{
		out_file<<"["<<it1_2.getValue().t1;
		out_file<<",'"<<produse.getValue(it1_2.getValue().t1).getName()<<"'";
		out_file<<","<<it1_2.getValue().t2;
		out_file<<","<<produse.getValue(it1_2.getValue().t1).getPrice()
			*(unsigned long long)(it1_2.getValue().t2);
		out_file<<"],\n";
	}

	input_file.open(index_input_file3, std::ifstream::in);
	if(!input_file)
	{
		out_file.close();
		return;
	}
	while(getline(input_file, line))
	{
		out_file<<line;
	}
	input_file.close();

	SinglyLinkedList< Pair<IdCategorie,unsigned int> >::Iterator it1_4;
	SinglyLinkedList< Pair<IdCategorie,unsigned int> > results1_4;

	for(unsigned i = 1 ; i <= magazine.getNrElm() ; i++)
	{
		results1_4 = SinglyLinkedList< Pair<IdCategorie,unsigned int> >();
		topCategorii(results1_4, categorii, produse, tranzactii, i);

		out_file<<"var dire_ex4_"<<i<<"= new google.visualization.DataTable();\n";
        out_file<<"dire_ex4_"<<i<<".addColumn('number', 'Id categorie');\n";
        out_file<<"dire_ex4_"<<i<<".addColumn('string', 'Nume categorie');\n";
        out_file<<"dire_ex4_"<<i<<".addColumn('number', 'Produse vandute');\n";
        out_file<<"dire_ex4_"<<i<<".addRows([\n";

		for(it1_4 = results1_4.begin() ; it1_4.isValid() ; ++it1_4)
		{
			out_file<<"["<<it1_4.getValue().t1;
			out_file<<",'"<<categorii.getCategorie(it1_4.getValue().t1)<<"'";
			out_file<<","<<it1_4.getValue().t2;
			out_file<<"],\n";
		}

		out_file<<"]);\n";
		out_file<<"drawTable1_4_"<<i<<"(dire_ex4_"<<i<<");\n";
		out_file<<"drawChart1_4_"<<i<<"(dire_ex4_"<<i<<");\n";
	}

	input_file.open(index_input_file4, std::ifstream::in);
	if(!input_file)
	{
		out_file.close();
		return;
	}
	while(getline(input_file, line))
	{
		out_file<<line;
	}
	input_file.close();

	for(unsigned i = 1 ; i <= magazine.getNrElm() ; i++)
	{
		out_file<<"function drawTable1_4_"<<i<<"(data) {\n";
		out_file<<"var table = new google.visualization.Table(document.getElementById('table_div_1_4_"<<i<<"'));\n";
		out_file<<"table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});}\n";

		out_file<<"function drawChart1_4_"<<i<<"(data2) { data = data2.clone(); data.removeColumn(0);\n";
      	out_file<<"var view = new google.visualization.DataView(data); var options = { chart: {\n";
        out_file<<"title: 'Company Performance', } };\n";
		out_file<<"var chart = new google.visualization.ColumnChart(document.getElementById(\"columnchart_1_4_"<<i<<"\"));\n";
      	out_file<<"chart.draw(view, options);}\n";
	}

	input_file.open(index_input_file5, std::ifstream::in);
	if(!input_file)
	{
		out_file.close();
		return;
	}
	while(getline(input_file, line))
	{
		out_file<<line;
	}
	input_file.close();

	out_file<<"\nValoarea cosului mediu este: <b>";
	out_file<<valoareCosMediu(produse,tranzactii)<<"</b>\n";

	input_file.open(index_input_file6, std::ifstream::in);
	if(!input_file)
	{
		out_file.close();
		return;
	}
	while(getline(input_file, line))
	{
		out_file<<line;
	}
	input_file.close();

	for(unsigned i = 1 ; i <= magazine.getNrElm() ; i++)
	{
		out_file<<"\n<b>Magazin "<<i<<" - "<<magazine.getMagazin(i)<<"</b><br>\n";
		out_file<<"<div id=\"table_div_1_4_"<<i<<"\" style=\"height: 150px;width:50%; display: inline-block\">\n";
		out_file<<"</div><div id=\"columnchart_1_4_"<<i<<"\" style=\"height: 150px;width:50%; display: inline-block\"></div>\n";
	}

	input_file.open(index_input_file7, std::ifstream::in);
	if(!input_file)
	{
		out_file.close();
		return;
	}
	while(getline(input_file, line))
	{
		out_file<<line;
	}
	input_file.close();



	out_file.close();
}
