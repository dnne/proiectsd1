#ifndef _HASH_FUNCTIONS_HEDER_
#define _HASH_FUNCTIONS_HEDER_

// Functie de hash cu numar buckets egal cu 2 ^ bit
unsigned int func_x64(unsigned long long key, unsigned bit);

// Functia de hash pentru perechile de produse - task 5 director executiv
unsigned int hash_prod_pair(unsigned long long key, unsigned buckets);

// Functia de hash pentru id-urile bonurilor
unsigned int hash_id_bon(unsigned long long key, unsigned buckets);

// Functia de hash pentru id-urile produselor
unsigned int hash_id_produs(unsigned key, unsigned buckets);

// Functia de hash pentru id-urile produselor
unsigned int hash_timp_unix(int key, unsigned buckets);


#endif
