#include "solve.h"


/* Functia ce rezolva primul task de la directorii executivi.
   Rezultatul functiei este intors sub forma unei liste(results) de tripleti
   (id magazin, nr produse vandute, suma totala incasata din vanzari), sortata
   dupa id-ul magazinului
 */
void vanzariMagazine(
	SinglyLinkedList< Triplet<IdMagazin,unsigned int,unsigned long long> >& results,
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii)
{

	HashMap<IdBon,Tranzactie>::Iterator it;
	SinglyLinkedList< Triplet<IdMagazin,unsigned int,unsigned long long> >::Iterator
		crtListIt, nextListIt;
	Triplet<IdMagazin,unsigned int,unsigned long long> triplet;

	unsigned long long suma; // sumata totala incasata din vanzari

	unsigned i;
	IdMagazin id_magazin; // id-ul magazinului
	unsigned nr_prod; // nr de produse vandute de catre magazinul curent

	bool found;

	// Parcurgearea tranzactiilor - bonurilor
	for(it = tranzactii.begin() ; it.isValid() ; ++it)
	{
		suma = 0;
		nr_prod = 0;

		// Parcurgerea produselor de pe bonuri
		for(i = 0 ; i < it.getValue().produse.getNrElm() ; i++)
		{
			// calcularea sumei obtinute prin vanzarea produselor respective
			// (de acelasi tip)
			suma += produse.getValue(it.getValue().produse[i].t1).getPrice() *
				it.getValue().produse[i].t2;

			// adaugare la numarul de produse pandute de catre magazinul care
			// a emis bonul
			nr_prod += it.getValue().produse[i].t2;
		}

		id_magazin = it.getValue().get_id_magazin();

		// Verificare daca lista de treipleti este goala sau daca id-ul
		// magazinului curent este mai mic decat cel al primul triplet, caz in
		// care trebuie sa punem tripletul la inceputul listei (dorim ca lista
		// sa fie sortata dupa id-ul magazinului )
		crtListIt = results.begin();
		if(crtListIt.isValid() == false ||
			crtListIt.getValue().t1 > id_magazin)
		{
			triplet.t1 = id_magazin;
			triplet.t2 = nr_prod;
			triplet.t3 = suma;
			results.addFirst(triplet);
			continue;
		}

		// Daca id-ul magazinului coincide cu cel al primului triplet se aduna
		// valorile obtinute anterior la valorile retinute in triplet
		if(crtListIt.getValue().t1 == id_magazin)
		{
			crtListIt.getValue().t2 += nr_prod;
			crtListIt.getValue().t3 += suma;
			continue;
		}

		found = false;

		// Cautarea magazinului prin lista, verificand de fiecare data id-ul
		// magazinului din tripletul urmator. Acest lucru este necesar
		// intrucat adaugarea elementelor se face dupa elementul curent. Cazul
		// in care id-ul magazinului trebuia adaugat la inceput a
		// fost tratat anterior
		for(nextListIt = ++(results.begin()) ;
			nextListIt.isValid() ; ++nextListIt, ++crtListIt)
		{
			// Daca a fost gasit magazinul in tripletul urmator se aduna
			// valorile obtinute anterior la valorile retinute in triplet
			if(nextListIt.getValue().t1 == id_magazin)
			{
				nextListIt.getValue().t2 += nr_prod;
				nextListIt.getValue().t3 += suma;
				found = true;
				break;
			}

			// Daca id-ul magazinului este intre cel curent si cel urmator
			// (id-ul magazinului curent din list < id_magazin si
			// id_magazin < id-ul magazinului urmator)
			// aceasta este pozita unde trebuie sa punem tripletul
			// corespunzator magazinului curent.
			if(nextListIt.getValue().t1 > id_magazin)
			{
				triplet.t1 = id_magazin;
				triplet.t2 = nr_prod;
				triplet.t3 = suma;
				crtListIt.addAfter(triplet);
				found = true;
				break;
			}

		}
		if(found == true)
			continue;

		// Daca nu am reusit sa gasim magazinul si a fost terminata
		// parcurgerea listei (id_magazin > id-ul ultimului magazin)
		// se adauga tripletul la final

		triplet.t1 = id_magazin;
		triplet.t2 = nr_prod;
		triplet.t3 = suma;
		results.addLast(triplet);
	}
}

/* Rezolvarea task-ului 2 de la directorii executivi
   Rezultatul este intros sub forma unei liste(results) de perechi
   (id produs, numarul de produse de acest tip vandute),
   sortata dupa id-ul produsului
*/
void vanzariProduse(
   SinglyLinkedList< Pair<IdProdus,unsigned int> >& results,
   HashMap<IdProdus,Produs>& produse,
   HashMap<IdBon,Tranzactie>& tranzactii)
{
	HashMap<IdBon,Tranzactie>::Iterator it;
	HashMap<IdProdus,Produs>::Iterator prodIt;
	SinglyLinkedList< Pair<IdProdus,unsigned int> >::Iterator
		crtListIt;
	Pair<IdProdus,unsigned int> pair;

	unsigned i;
	IdProdus id_prod; // id-ul produsului curent
	unsigned nr_prod; // numarul de produse de tip id_prod vandute

	// Adaugarea tuturor produselor in lista
	for(prodIt = produse.begin() ; prodIt.isValid() ; ++prodIt)
	{
		pair.t1 = prodIt.getKey();
		pair.t2 = 0;
		results.addSorted(pair,compare_pair_prod_sales);
	}

	// Parcurgerea tranzactiilor
	for(it = tranzactii.begin() ; it.isValid() ; ++it)
	{
		// Parcurgerea produselor
		for(i = 0 ; i < it.getValue().produse.getNrElm() ; i++)
		{
			id_prod = it.getValue().produse[i].t1;
			nr_prod = it.getValue().produse[i].t2;

			// Parcurgerea listei si adunarea produselor
			for(crtListIt = results.begin() ;
				crtListIt.isValid() ; ++crtListIt)
			{
				if(crtListIt.getValue().t1 == id_prod)
				{
					crtListIt.getValue().t2 += nr_prod;
					break;
				}
			}
		}
	}
}

/* Rezolvarea taskului 3 de la directorii executivi
	Rezultatul este intors sub forma de numar real (double)
*/
double valoareCosMediu(
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii)
{
	// Valoarea totala a tranzactiilor - suma totala incasata din vanzari
	unsigned long long sum = 0;

	unsigned int nr_tranz = 0; // Numarul de tranzactii
	unsigned int i;

	HashMap<IdBon,Tranzactie>::Iterator it;

	// Parcurgerea tranzactiilor
	for(it = tranzactii.begin() ; it.isValid() ; ++it)
	{
		// Parcurgearea produselor de pe fiecare bon - tranzactie
		for(i = 0 ; i < it.getValue().produse.getNrElm() ; i++)
		{
			// Calcularea sumei incasate de pe vanzarea produselor de tipul
			// curent de pe tranzactia curenta si adunarea ei la suma totala
			// incasata
			sum += produse.getValue(it.getValue().produse[i].t1).getPrice() *
				it.getValue().produse[i].t2;
		}

		// Incrementarea numarului de tranzactii
		nr_tranz++;
	}

	if(nr_tranz == 0)
		return 0.0;

	return double(sum)/nr_tranz;
}

/* Rezolvarea task-ului 4 de la directorii executivi, pentru un anumit magazin
   transmis ca parametru (id_magazin).
   Rezultatul este stocat sub forma unei liste(results) de perechi
   (categorie, numar de produse din categorie vandute), sortata dupa numarul de
   produse vandute.
*/
void topCategorii(
	SinglyLinkedList< Pair<IdCategorie,unsigned int> >& results,
	Categorii& categorii,
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii,
	IdMagazin id_magazin)
{

	HashMap<IdBon,Tranzactie>::Iterator it;
	unsigned int i;
	IdProdus id_prod; // id-ul produsului curent
	Pair<IdCategorie,unsigned int> pair;

	// Vectorul categoriilor
	// i = categoria i
	// prod_vand[i-1] = numarul de produse din categoria i vandute
	ResizableArray<unsigned> prod_vand(categorii.getNrElm());

	// Initial nu avem produse vandute
	for(i = 0 ; i < categorii.getNrElm() ; i++)
		prod_vand.add(0);

	// Parcurgerea tranzactiilor
	for(it = tranzactii.begin() ; it.isValid() ; ++it)
	{
		// Se ignora tranzactiile realizate de alte magazine decat cel curent
		if(it.getValue().get_id_magazin() != id_magazin)
			continue;

		// Parcurgerea produselor de pe tranzactii
		for(i = 0 ; i < it.getValue().produse.getNrElm() ; i++)
		{
			id_prod = it.getValue().produse[i].t1;

			// adunarea numarului de produse de tipul curent la numarul
			// de produse din categoria corespunzatoare
			prod_vand[produse.getValue(id_prod).getId_cat() - 1] +=
				it.getValue().produse[i].t2;
		}
	}

	// Parcurgerea categoriilor
	for(i = 0 ; i < categorii.getNrElm() ; i++)
	{
		// Ignorarea categoriilor nevandute
		//if(prod_vand[i] == 0)
		//	continue;

		pair.t1 = i+1; // id-ul categoriei este cu 1 mai mare decat indicele
		pair.t2 = prod_vand[i];

		// Adaugare in lista sortata dupa numarul de produse din categorie
		// vandute
		results.addSorted(pair, compare_pair_categ_sales);
	}

}

/* Rezolvarea taskului 5 de la directorii executivi
   Rezultatul este transmis sub forma unei liste(results) de tripleti
   (id produs 1 , id produs 2 , numarul de perechi)
*/
void topPerechi(
	SinglyLinkedList< Triplet<IdProdus,IdProdus,unsigned int> >& results,
	HashMap<IdBon,Tranzactie>& tranzactii)
{
	HashMap<IdBon,Tranzactie>::Iterator it;
	unsigned int i,j,
		nr_produse1,nr_produse2;
	IdProdus id_produs1, id_produs2;
	unsigned long long key;
	Triplet<IdProdus,IdProdus,unsigned int> triplet;

	// Hashtable cu cheia egala cu concatenarea bitilor id-urilor produselor
	// cu specificatia ca primul dintre id-uri este obligatoriu mai mic decat
	// cel de-al doilea
	// Valoarea este numarul de perechi
	HashMap<unsigned long long,unsigned int>
		prod_pair_sales(NR_PRODUSE*(NR_PRODUSE-1)/2, hash_prod_pair);

	HashMap<unsigned long long,unsigned int>::Iterator pair_it;

	// Parcurgerea tranzactiilor
	for(it = tranzactii.begin() ; it.isValid() ; ++it)
	{
		// Parcugerea perechilor de produse de pe fiecare tranzactie - bon
		for(i = 0 ; i < it.getValue().produse.getNrElm() -1 ; i++)
		{
			for(j = i+1 ; j < it.getValue().produse.getNrElm() ; j++)
			{
				// Obtinerea id-urilor produselor de pe pozitiile i si j
				id_produs1=it.getValue().produse[i].t1;
				id_produs2=it.getValue().produse[j].t1;

				// Calcularea cheii - concatenarea bitilor folositi pentru
				// reprezentarea celor doua id-uri ale produselor, primul fiind
				// id-ul mai mic
				key  = (unsigned long long)
					(id_produs1 <= id_produs2 ? id_produs1 : id_produs2) << 32;
				key |= id_produs1 <= id_produs2 ? id_produs2 : id_produs1;

				// Obtinerea numarului de produse de tip id_produs...
				nr_produse1 = it.getValue().produse[i].t2;
				nr_produse2 = it.getValue().produse[j].t2;

				// Incercarea de a aduna la valuarea asociata checii
				// minimul dintre numerele de produse
				try{
					// Daca pereche a fost adaugata anterior operatia va fi
					// executata cu succes
					prod_pair_sales.getValue(key) +=
						(nr_produse1 <= nr_produse2 ? nr_produse1 : nr_produse2);
				}
				catch(...)
				{
					// In caz contrar functia executata in blocul 'try'
					// va arunca o exceptie ce va fi prinsa in blocul 'catch'
					// Daca perechea nu a fost adaugata anterior in hashtable
					// va fi adaugata acum
					prod_pair_sales.add(key,
						nr_produse1 <= nr_produse2 ? nr_produse1 : nr_produse2);
				}
			}
		}
	}

	// Parcurgerea perechilor(K,V) din hashtable
	for(pair_it = prod_pair_sales.begin() ; pair_it.isValid() ; ++pair_it)
	{
		// Obtinerea cheii
		key = pair_it.getKey();

		// Obtinerea id-ului primul produs(cel cu id mai mic) pe baza cheii
		triplet.t1 = key>>32;

		// Obtinerea id-ului celui de-al doilea produs(cel mai mare)
		// pe baza cheii
		triplet.t2 = key&0xFFFFFFFF;

		// Obtinearea numarului de perechi vandute - valoarea asociata perechii
		// din hashtable
		triplet.t3 = pair_it.getValue();

		// Adugarea tripletului in lista sortata dupa numarul de perechi vandute
		results.addSorted(triplet, compare_triplet_prod_sales);
	}

}

/* Rezolvarea taskului 3 de la directorii magazine
   Rezultatul este transmis sub forma unei liste(results) de tripleti
   (nume produs , cantitate produs , pret produs)
*/
void obtineBon(Tranzactie& tranz_curent,
               HashMap<IdProdus,Produs>& produse,
               SinglyLinkedList< Triplet<char *,unsigned int,unsigned int> >& results)
{
        Triplet<char *,unsigned int,unsigned int> triple;
        IdProdus id_prod;

        for(unsigned int i = 0 ; i < tranz_curent.produse.getNrElm() ; i++ )
        {
                //extragem id-ul produsului
                id_prod = tranz_curent.produse[i].t1;

                //extragem numele produsului
                triple.t1=produse.getValue(id_prod).getName();

                //extragem numarul de produse cimparate
                triple.t2 = tranz_curent.produse[i].t2;

                //extragem pretul produsului
                triple.t3=produse.getValue(id_prod).getPrice();

                results.addLast(triple);

        }
}
/* Rezolvarea taskului 4 de la directorii magazine
   Rezultatul este transmis sub forma unui vector de tipul
   (results[id_magazin-1]=cati_clieti_beneficiaza)
*/
void catiClienti(Magazine& magazine,
				HashMap<IdBon,Tranzactie>& tranzactii,
				int beneficiari[])
{
	int * last_time,diff,id_magazin;
	unsigned int i;

	last_time= new int[magazine.getNrElm()];
	HashMap<IdBon,Tranzactie>::Iterator it;
	SinglyLinkedList<Tranzactie> transort;
	SinglyLinkedList<Tranzactie>::Iterator ord;
	for(i=0;i<magazine.getNrElm();++i)
	{
		last_time[i]=0;
		beneficiari[i]=0;
	}
	for(it=tranzactii.begin();it.isValid();++it)
	{
		transort.addSorted(it.getValue(),comparare_tranzactii);
	}
	for(ord=transort.begin();ord.isValid();++ord)
	{
		id_magazin=ord.getValue().get_id_magazin()-1;
		diff=ord.getValue().get_time().convertToUnix();
		diff-=last_time[id_magazin];
		/*diferenta de timp intre tranzactia curenta si cea anterioara*/
		diff-=TIME_PER_TRANZ;
		for(i=0;i<ord.getValue().produse.getNrElm();++i)
		{
			diff-=TIME_PER_PROD*ord.getValue().produse[i].t2;
		}
		if(diff<=0) 
		{
			++beneficiari[id_magazin];
		}
		last_time[id_magazin]=ord.getValue().get_time().convertToUnix();
	}
	delete[] last_time;
}

/*void ordonare_tranzactie(SinglyLinkedList<Tranzactie> &sort_tranzactii,
       HashMap<IdBon,Tranzactii> &tranzactii)
{
       HashMap<IdBon,Tranzactii>::Iterator it;
       for(it=tranzactii.begin();it.isValid();++it)
       {
               sort_tranzactii.addSorted(it.getValue(),comparare_tranzactii);
       }

}

void initializare_stoc(ResizableArray< ResizableArray< unsigned int > > &stocuri_pe_magazine,
                                               Magazine& magazine)
{
       stocuri_pe_magazine.setNrElm(magazine.getNrElm());
}
*/

