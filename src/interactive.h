#ifndef _INTERACTIVE_HEADER_
#define _INTERACTIVE_HEADER_

#include "generic_data_structures/hash_map.h"
#include "Clase/Categorii.h"
#include "Clase/Magazine.h"
#include "Clase/Produs.h"
#include "Clase/Tranzactie.h"
#include "Clase/Palet.h"
#include "solve.h"
#include "typedefs.h"

void menuMain(Categorii& categorii,
	Magazine& magazine,
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii,
	ResizableArray< SinglyLinkedList<IdSlot>>& sloturi_pe_produse,
	ResizableArray< SinglyLinkedList<Triplet <IdPalet, IdProdus, NrItems > > > &paleti_pe_sloturi
	);

#endif