#ifndef _LOAD_HEADER_
#define _LOAD_HEADER_

#include <string>
#include "Clase/Magazine.h"
#include "Clase/Categorii.h"
#include "Clase/Produs.h"
#include "Clase/Tranzactie.h"
#include "Clase/Palet.h"
#include "generic_data_structures/hash_map.h"
#include "generic_data_structures/singly_linked_list.cpp"
#include "generic_data_structures/Stack.h"
#include "typedefs.h"
#include "./generic_data_structures/pair.h"

const std::string magazine_csv="input/magazine.csv";
const std::string tranzactii_csv="input/tranzactii.csv";
const std::string bonuri_csv="input/bonuri.csv";
const std::string categorii_csv="input/categorii.csv";
const std::string produse_csv="input/produse.csv";
const std::string paleti_csv="input/paleti.csv";
const std::string file_token=",";

extern bool isMagazineLoaded;
extern bool isTranzactiiLoaded;
extern bool isBonuriLoaded;
extern bool isProduseLoaded;
extern bool isCategoriiLoaded;
extern bool isPaletiLoaded;

bool loadMagazine(Magazine& magazine);
bool loadCategorii(Categorii& categorii);
bool loadProduse(HashMap<unsigned,Produs>& produse);
bool loadTranzactii(HashMap<unsigned long long,Tranzactie>& tranzactii);
bool loadPaleti(ResizableArray< SinglyLinkedList<IdSlot> > &sloturi_pe_produse,
				ResizableArray< SinglyLinkedList<Triplet <IdPalet, IdProdus, NrItems > > > &paleti_pe_sloturi);





#endif