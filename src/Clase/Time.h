/*O stii pe aia cu timpul?
convert-primeste data ca char * si o converteste in an, ziua din an si secunda din zi
getYear-intoarce anul
getDay-intoarce ziua
getSec-intoarce secunda
getHour-intoarce ora
getMin-intoarce minutul
getDayoftheweek-intoarce ziua din saptamana
					0-luni
					1-marti
					2-miercuri
					3-joi
					4-vineri
					5-sambata
					6-duminica
*/
#ifndef __TIME__H
#define __TIME__H

#include<iostream>
#include<string>
#include<stdlib.h>
#include<cstring>
#include<time.h>
class Time
{
private:
	int an, zi_din_an, secunda_din_zi,zi_din_luna,luna;
public:
	Time()
	{
		an=1970;
		zi_din_an=1;
		secunda_din_zi=0;
		zi_din_luna=1;
		luna=1;
	}
	~Time()
	{
	}
	void convert(const char data[25])
	{
		int bisect,i,j,ian=0,feb=31,mar=59,apr=90,mai=120,iun=151,iul=181,aug=212,sep=243,oct=273,nov=304,dec=334;
		char aux[10];
		i=0;
		while(data[i]!='-')
		{
			aux[i]=data[i];
			++i;
		}
		aux[i]='\0';
		bisect=0;
		an=atoi(aux);
		if(an%4==0) bisect =1;
		++i;
		j=0;
		while(data[i]!='-')
		{
			aux[j]=data[i];
			++j;
			++i;
		}
		aux[j]='\0';
		if(strcmp(aux,"01")==0)
			{
				zi_din_an=ian;
				luna=1;
			}
		if(strcmp(aux,"02")==0)
			{
				zi_din_an=feb;
				luna=2;
			}
		if(strcmp(aux,"03")==0)
			{
				zi_din_an=mar+bisect;
				luna=3;
			}
		if(strcmp(aux,"04")==0)
			{
				zi_din_an=apr+bisect;
				luna=4;
			}
		if(strcmp(aux,"05")==0)
			{
				zi_din_an=mai+bisect;
				luna=5;
			}
		if(strcmp(aux,"06")==0)
			{
				zi_din_an=iun+bisect;
				luna=6;
			}
		if(strcmp(aux,"07")==0)
			{
				zi_din_an=iul+bisect;
				luna=7;
			}
		if(strcmp(aux,"08")==0)
			{
				zi_din_an=aug+bisect;
				luna=8;
			}
		if(strcmp(aux,"09")==0)
			{
				zi_din_an=sep+bisect;
				luna=9;
			}
		if(strcmp(aux,"10")==0)
			{
				zi_din_an=oct+bisect;
				luna=10;
			}
		if(strcmp(aux,"11")==0)
			{
				zi_din_an=nov+bisect;
				luna=11;
			}
		if(strcmp(aux,"12")==0)
			{
				zi_din_an=dec+bisect;
				luna=12;
			}
		j=0;
		++i;
		while(data[i]!=' ')
		{
			aux[j]=data[i];
			++j;
			++i;
		}
		aux[j]='\0';
		zi_din_an+=atoi(aux);
		zi_din_luna=atoi(aux);
		++i;
		j=0;
		while(data[i]!=':')
		{
			aux[j]=data[i];
			++j;
			++i;
		}
		aux[j]='\0';
		secunda_din_zi=atoi(aux)*3600;
		++i;
		j=0;
		while(data[i]!=':')
		{
			aux[j]=data[i];
			++j;
			++i;
		}
		aux[j]='\0';
		secunda_din_zi+=atoi(aux)*60;
		++i;
		j=0;
		while(data[i]!='\0')
		{
			aux[j]=data[i];
			++j;
			++i;
		}
		aux[j]='\0';
		secunda_din_zi+=atoi(aux);
		++i;
		j=0;
	}
	int getDayoftheweek()
	{
		int dayofweek=5,bisecti;
		dayofweek+=an-1972;
		bisecti=(an-1972)/4+1;
		dayofweek+=bisecti;
		if(an%4==0) --dayofweek;
		dayofweek+=zi_din_an-1;
		dayofweek=dayofweek%7;
		return dayofweek;
	}
	void print()
	{
		std::cout<<"\nAn= "<<an<<"\nZiua din an= "<<zi_din_an<<"\nSecunda din zi= "<<secunda_din_zi;
	}
	int getYear()
	{
		return an;
	}
	int getDay()
	{
		return zi_din_luna;
	}
	int getSec()
	{
		return secunda_din_zi%60;
	}
	int getMonth()
	{
		return luna;
	}
	int getHour()
	{
		return secunda_din_zi/3600;
	}
	int getMin()
	{
		return (secunda_din_zi%3600)/60;
	}
	int getSecOfTheDay()
	{
		return secunda_din_zi;
	}
	int getDayOfTheYear()
	{
		return zi_din_an;
	}
	friend bool operator<(const Time &t1,const Time &t2)
	{
		if(t1.an<t2.an) return true;
		if(t1.an>t2.an) return false;
		if(t1.zi_din_an<t2.zi_din_an) return true;
		if(t1.zi_din_an>t2.zi_din_an) return false;
		if(t1.secunda_din_zi<t2.secunda_din_zi) return true;
		if(t1.secunda_din_zi>t2.secunda_din_zi) return false;
		return false;
	}
	int convertToUnix()
	{
	        struct tm t;
                time_t t_of_day;
                t.tm_sec = getSec();
                t.tm_min = getMin();
                t.tm_hour = getHour();
                t.tm_mon = getMonth()-1;
                t.tm_year = getYear()-1900;
                t.tm_mday = getDay();
                t.tm_isdst = -1;// Is DaylightSavingTime on? 1 = yes, 0 = no, -1 = unknown

                t_of_day = mktime(&t);
                return (int) t_of_day;
	}

};
#endif
