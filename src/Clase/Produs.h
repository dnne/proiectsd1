/*Implementarea clasei "Produs"*/
#ifndef __PRODUS__H
#define __PRODUS__H
#include <string.h>
#include <iostream>
#include "../typedefs.h"

class Produs
{
private:
	IdCategorie id_cat;
	int pret;
	char nume[30];
public:
	Produs(){
		id_cat = 0;
		pret = 0;
		nume[0] = 0;
	}
	Produs(char nume[30], IdCategorie id_cat, int pret)
	{
		this->id_cat=id_cat;
		this->pret=pret;
		strcpy(this->nume,nume);
	}
	void Set_nume(const char nume[30])
	{
		strcpy(this->nume,nume);
	}
	void Set_cat_id(IdCategorie id_cat)
	{
		this->id_cat=id_cat;
	}
	void Set_pret(int pret)
	{
		this->pret=pret;
	}
	void Print()
	{
		std::cout<<nume<<'\n'<<id_cat<<'\n'<<pret<<'\n';
	}
	IdCategorie getId_cat()
	{
		return id_cat;
	}
	int getPrice()
	{
		return pret;
	}
	char * getName()
	{
		return nume;
	}	

};
#endif