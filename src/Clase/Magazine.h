#ifndef _MAGAZINE_HEADER_
#define _MAGAZINE_HEADER_
#include <iostream>
#include "../generic_data_structures/resizable.h"
#include "../typedefs.h"

class Magazine 
{
private:
	ResizableArray<std::string> locatii;

public:

	Magazine();

	~Magazine();

	void addMagazin (std::string magazin_nou);
	
	std::string getMagazin (IdMagazin id);

	unsigned getNrElm();

};


#endif