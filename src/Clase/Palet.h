#ifndef _PALET_
#define _PALET_
#include <iostream>

class Palet {
private:
	std::string id;
	unsigned int prod_type;
	unsigned int n_items;
	unsigned int slot;
public:
	unsigned int getSlot()
	{
		return slot;
	}
	unsigned int getProd_type()
	{
		return prod_type;
	}
	unsigned int getN_items()
	{
		return n_items;
	}
	std::string getId()
	{
		return id;
	}
	//Următorul constructor va iniţializa membrii de tip int cu 0, iar string-ul cu "".
	Palet(){}

	//Constructor cu parametrii
	Palet(std::string id, unsigned int prod_type, unsigned int n_items, unsigned int slot)
	{
		this->id = id;
		this->prod_type = prod_type;
		this->n_items = n_items;
		this->slot = slot;
	}
	//Copy constructor
	Palet(const Palet& palet){
		id = palet.id;
		prod_type = palet.prod_type;
		n_items = palet.n_items;
		slot = palet.slot;
	}
	//Assignment operator overloading
	void operator = (const Palet& palet){
		id = palet.id;
		prod_type = palet.prod_type;
		n_items = palet.n_items;
		slot = palet.slot;
	}

	//Destructor
	~Palet(){}

	void setslot(unsigned int x)
	{
		this->slot = x;
	}

	void setprod_type(unsigned int x)
	{
		this->prod_type = x;
	}

	void setn_items(unsigned int x)
	{
		this->n_items = x;
	}

	void setId(std::string x)
	{
		this->id = x;
	}
	//Only for test functionality
	void print()
	{
		std::cout << "Id = " << id << "\nprod_type: " << prod_type;
		std::cout << "\nn_items: " << n_items << "\nSlot: " << slot << "\n\n";
	}

};

#endif // _PALET_
