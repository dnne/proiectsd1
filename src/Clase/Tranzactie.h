/*Implementarea clasei "Tranzactie"*/
#ifndef __TRANZACTIE_
#define __TRANZACTIE_

#include "Time.h"
#include "../generic_data_structures/pair.h"
#include <cstring>
#include "../generic_data_structures/resizable.cpp"
#include "Produs.h"
#include "../typedefs.h"

#define DEFAULT_NR_PRODUSE 1

class Tranzactie {
	private:
		Time time;
		IdMagazin id_magazin;
		IdTranzactie id_tranzactie;
	public:	
		// Pair<id produs, numar produse>
		ResizableArray< Pair<IdProdus,unsigned int> > produse;

		Tranzactie()
		{
			produse = ResizableArray< Pair<IdProdus,unsigned int> >(DEFAULT_NR_PRODUSE);
		}
		
		void setIdMagazin(IdMagazin id_m) {
			id_magazin = id_m;
		}

		void setTime(const char timestamp[25]) {
			time.convert(timestamp);
		}

		IdMagazin get_id_magazin() {
			return id_magazin;
		}

		Time get_time() const{
			return time;
		}

		void setIdTranzactie(IdTranzactie id)
		{
			id_tranzactie = id;
		}

		IdTranzactie getIdTranzactie()
		{
			return id_tranzactie;
		}

		void addProdus(IdProdus id_produs)
		{
			unsigned i;
			for(i = 0 ; i< produse.getNrElm() ; i++)
			{
				if(produse[i].t1 == id_produs)
				{
					produse[i].t2++;
					return;
				}
			}

			Pair<IdProdus,unsigned> pair;
			pair.t1 = id_produs;
			pair.t2 = 1;
			produse.add(pair);
		}
};
#endif // __TRANZACTIE_
