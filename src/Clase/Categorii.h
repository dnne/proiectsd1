#ifndef _CATEGORII_HEADER_
#define _CATEGORII_HEADER_
#include "../generic_data_structures/resizable.h"
#include "../typedefs.h"


class Categorii
{
	private:
		ResizableArray<std::string> nume ;
	public:
		Categorii();
		~Categorii();
		void addCategorie(std::string nume);
		std::string getCategorie(IdCategorie id);
		unsigned getNrElm();
};

#endif
