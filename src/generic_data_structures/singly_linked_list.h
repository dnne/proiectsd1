/*
    SinglyLinkedList

    Notes:
    -Daca puteti alege intre removeFirst() sau removeLast() alegeti removeFirst()
    -removeFirst() si removeLast() arunca o exceptie inca cazul in care nu 
     sunt elemente in lista. Vezi exemplu
*/



#ifndef _SINGLY_LINKED_LIST_HEADER_
#define _SINGLY_LINKED_LIST_HEADER_

#include <iostream>

template <typename T> class SinglyLinkedList;

template <typename T>
std::ostream& operator<<(std::ostream& out, const SinglyLinkedList<T>& list);

//o parte din scheletul clasei SinglyLinkedList
//     - http://ocw.cs.pub.ro/courses/sd-ca/articole/articol-06
template <typename T>
class SinglyLinkedList{
private:
    class Node;
    Node *pFirst, *pLast;
public: 
    // Constructor
    SinglyLinkedList();
    // Destructor
    ~SinglyLinkedList();
 
    /* Adauga un nod cu valoarea == value la inceputul listei. */
    void addFirst(const T& value);
 
    /* Adauga un nod cu valoarea == value la sfarsitul listei. */
    void addLast(const T& value);

    /* Adauga un nod cu valoarea == value la pozitia corespunzatoarea astfel
       incat vectorul sa ramana sortat. */
    void addSorted(const T& value, int (*compareFunc)(const T&,const T&));
 
    /* Elimina elementul de la inceputul listei si intoarce valoarea acestuia. */
    T removeFirst() throw(int);

    /*Consultă primul element din listă, fără a-l şterge*/
    T peekFirst() throw(int);

 
    /* Elimina elementul de la sfarsitul listei listei si intoarce valoarea acestuia. */
    T removeLast() throw(int);
 
    /* Elimina prima aparitie a elementului care are valoarea == value. */
    //T removeFirstOccurrence(T value);
 
    /* Elimina ultima aparitie a elementului care are valoarea == value. */
    //T removeLastOccurrence(T value);
 
    /* Afiseaza elementele listei pe o singura linie, separate printr-un spatiu. */
    friend std::ostream& operator<< <>(std::ostream& out, const SinglyLinkedList<T>& list);

    SinglyLinkedList<T>& operator=(const SinglyLinkedList<T>& list);
 
    /* Intoarce true daca lista este vida, false altfel. */
    bool isEmpty();

    class Iterator;

    Iterator begin();
    Iterator end();
};

template <typename T>
class SinglyLinkedList<T>::Iterator
{
private:
    Node *pNode;
    SinglyLinkedList<T>* pList;

public:
    Iterator();

    T& getValue() throw(int);

    //Preincrementare
    Iterator& operator++();

    //Postincrementare
    Iterator operator++(int);

    bool addAfter(const T& value);
    bool removeAfter();

    bool operator==(Iterator& it);
    bool operator==(Iterator it);
    bool operator!=(Iterator& it);
    bool operator!=(Iterator it);

    bool isValid();

    friend class SinglyLinkedList<T>;
};

template <typename T>
class SinglyLinkedList<T>::Node {
public:
    T value;
    Node *next;
    Node (const T value);
    Node();
};

#endif