/*
Implementarea stivei
Functii:
push - adauga un element nou in stiva
pop - scoate elementul aflat in varfyl stivei
peek - intoarce elementul din varful stivei
print - afiseaza continutul stivei
isEmpty - intoarce 1 daca stiva e goala sau 0 daca are cel putin un element
*/

#ifndef __STIVA__H
#define __STIVA__H

#include<iostream>

template<class T>
class Stack
{
private:
	T * stack;
	int cap,size;
public:
	Stack()
	{
		stack = new T[5];
		cap=5;
		size=0;
	}
	~Stack()
	{
		delete[] stack;
	}
	void push(T val)
	{
		if (size == cap) 
		{
			T *tmpstack = new T[2*cap];
			for ( int i = 0; i < size; ++i ) 
			{
				tmpstack[i]=stack[i];
			}
			delete[] stack;
			stack=tmpstack;
			cap=2*cap;
		}
		stack[size]=val;
		size++;
	}
	T peek()
	{
		return stack[size-1];
	}
	T pop()
	{
		if(size<=cap/2)
		{
			T *tempstack = new T[cap/2];
			for(int i=0;i<size;++i)
			{
				tempstack[i]=stack[i];
			}
			delete[] stack;
			stack=tempstack;
			cap=cap/2;
		}
		size --;
	}
	int isEmpty()
	{
		if(size) return 0;
		return 1;
	}
	void print()
	{
		int i;
		for(i=0;i<size;++i)
			std::cout<<stack[i]<<' ';
	}
};
#endif