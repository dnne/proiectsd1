#ifndef _HEADER_RESIZEABLE_H_
#define _HEADER_RESIZEABLE_H_

#include <fstream>
#include "Stack.h"

template <typename T>
class ResizableArray;

template <typename T>
std::ostream& operator<<(std::ostream& f, const ResizableArray<T>& r);

template <typename T>
class ResizableArray{

private:
	unsigned size;
	unsigned nr_elm;
	T *v;

public:
	ResizableArray();
	ResizableArray(unsigned size);
	~ResizableArray();
	ResizableArray(const ResizableArray &a);
	friend std::ostream& operator<< <>(std::ostream& f, const ResizableArray<T>& r);
	ResizableArray<T>& operator=(const ResizableArray<T>& a);
	T& operator[](int index) throw(int);
	void add(T x);
	unsigned getCapacity();
	unsigned getNrElm();
	//bool remove_element(T x);
	void setNrElm(unsigned size);
};

//template class ResizableArray<int>;

//template std::ostream& operator<< <int>(std::ostream&, const ResizableArray<int>&);

#endif
