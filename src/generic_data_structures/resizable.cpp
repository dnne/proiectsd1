#include <fstream>
#include "resizable.h"
#include "pair.h"
#include "singly_linked_list.h"
#include "../Clase/Palet.h"

template <typename T>
ResizableArray<T>::ResizableArray()
{
	size=10;
	nr_elm=0;
	v = new T[size];
}

template <typename T>
ResizableArray<T>::ResizableArray(unsigned size)
{
	this->size=size;
	nr_elm=0;
	if(size != 0)
		v = new T[size];
	else
		v = NULL;
}

template <typename T>
ResizableArray<T>::~ResizableArray()
{
	if(v != NULL)
		delete[] v;
}

template <typename T>
std::ostream& operator<<(std::ostream& f, const ResizableArray<T>& r)
{
	f<<'(';
	if(r.nr_elm >= 1) f<<r.v[0];
	for(unsigned i=1 ; i < r.nr_elm ; i++ )
		f<<','<<r.v[i];
	f<<")\n";
	return f;		
}

template <typename T>
void ResizableArray<T>::add(T x)
{
	if(size == nr_elm)
	{
		size*=2;
		T *new_v=new T[size];
		for(unsigned i=0 ; i< nr_elm ; i++)
			new_v[i]=v[i];
		if(v != NULL)
			delete[] v;
		v = new_v;
	}
	v[nr_elm++]=x;
}
//contructor copy
template <typename T>
ResizableArray<T>::ResizableArray(const ResizableArray<T> &a)
{
	this->size=a.size;
	this->nr_elm=a.nr_elm;
	if(size != 0)
		this->v = new T[this->size];
	else
		this->v = NULL;
	for(unsigned i=0;i<a.nr_elm;i++)
		this->v[i]=a.v[i];
}
//operator de atribuire
template <typename T>
ResizableArray<T>& ResizableArray<T>::operator=(const ResizableArray<T>& a)
{
	if(v != NULL)
		delete[] this->v; // stergem vectorul deja artibuit
	this->size=a.size;
	this->nr_elm = a.nr_elm;
	if(size != 0)
		this->v = new T[size];
	else
		this->v = NULL;
	for(unsigned i=0 ; i<nr_elm ; i++)
		this->v[i]=a.v[i];
	return *this; //pentru atribuiri inlantuite
}

template <typename T>
T& ResizableArray<T>::operator[](int index) throw(int){
	if(nr_elm == 0)
		throw (int)0x1;

	if(index < 0)  index += (-index/nr_elm)*nr_elm + nr_elm;
	else index = index % nr_elm;

	return v[index];
}

template <typename T>
unsigned ResizableArray<T>::getCapacity(){
	return size;
}

template <typename T>
unsigned ResizableArray<T>::getNrElm(){
	return nr_elm;
}

/*template <typename T>
bool ResizableArray<T>::remove_element(T x)
{
	int elm_idx=-1;
	unsigned i;
	for(i = 0 ; i<nr_elm ; i++)
		if(v[i] == x) //caut elementul
		{
			elm_idx=i;
			break;
		}
	if(elm_idx == -1)
		return 0; 
	//l-am gasit => il sterg
	
	for(i = elm_idx ; i < nr_elm - 1 ; i++)
		v[i]=v[i+1];
	
	nr_elm--;

	if(size > 10 && (size / 2) >= nr_elm) //verifc daca pot micsora "size"
		size /= 2;
	return 1;
}*/

template <typename T>
void ResizableArray<T>::setNrElm(unsigned size)
{
	if(size == this->nr_elm)
		return;
	unsigned min_size = size < this->nr_elm ? size : this->nr_elm;
	T* new_v = new T[size];
	for(unsigned i = 0 ; i < min_size ; i++)
	{
		new_v[i] = v[i];
	}
	delete[] v;
	v = new_v;
	this->size = size;
	nr_elm = size;
}


template class ResizableArray<int>;
template class ResizableArray<std::string>;
template class ResizableArray< Pair<unsigned int,unsigned int> >;
template class ResizableArray<unsigned>;
template class ResizableArray< Stack<Palet> >;


template std::ostream& operator<<(std::ostream& f, const ResizableArray<int>& r);
template std::ostream& operator<<(std::ostream& f, const ResizableArray<std::string>& r);

