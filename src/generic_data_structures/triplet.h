#ifndef _TRIPLET_HEADER_
#define _TRIPLET_HEADER_

template <typename T1, typename T2, typename T3>
class Triplet
{
public:
	T1 t1;
	T2 t2;
	T3 t3;
};

#endif