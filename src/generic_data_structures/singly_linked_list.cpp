#include "singly_linked_list.h"
#include "pair.h"
#include "triplet.h"
#include "../Clase/Produs.h"
#include "../Clase/Tranzactie.h"
#include "../typedefs.h"

#include <cstdlib>


template <typename T>
SinglyLinkedList<T>::SinglyLinkedList()
{
	pFirst = pLast = NULL;
}

template <typename T>
SinglyLinkedList<T>::~SinglyLinkedList()
{
	SinglyLinkedList<T>::Node *pCrtNode, *pNextNode;
	if(pFirst == NULL)
		return;
	pCrtNode = pFirst;
	pNextNode = pCrtNode->next;

	while(pNextNode != NULL)
	{
		delete pCrtNode;
		pCrtNode = pNextNode;
		pNextNode = pCrtNode->next;
	}

	delete pCrtNode;
}

template <typename T>
void SinglyLinkedList<T>::addFirst(const T& value)
{
	SinglyLinkedList<T>::Node *pNode = new SinglyLinkedList<T>::Node(value);
	if(pFirst == NULL)
	{
		pFirst = pLast = pNode;
		return;
	}
	pNode->next = pFirst;
	pFirst = pNode;
}

template <typename T>
void SinglyLinkedList<T>::addLast(const T& value)
{
	SinglyLinkedList<T>::Node *pNode = new SinglyLinkedList<T>::Node(value);
	if(pFirst == NULL)
	{
		pFirst = pLast = pNode;
		return;
	}
	pLast->next = pNode;
	pLast = pNode;
}

template <typename T>
void SinglyLinkedList<T>::addSorted(const T& value,
	int (*compareFunc)(const T&,const T&))
{
	SinglyLinkedList<T>::Node *pNode = new SinglyLinkedList<T>::Node(value);
	if(pFirst == NULL)
	{
		pFirst = pLast = pNode;
		return;
	}

	if(compareFunc(pFirst->value,value) >= 0)
	{
		pNode->next = pFirst;
		pFirst = pNode;
		return;
	}

	SinglyLinkedList<T>::Node *pCrt;

	for(pCrt = pFirst ; pCrt->next != NULL ; pCrt = pCrt->next)
	{
		if(compareFunc(pCrt->next->value,value) >= 0)
		{
			pNode->next = pCrt->next;
			pCrt->next = pNode;
			return;
		}
	}

	pLast->next = pNode;
	pLast = pNode;

}

template <typename T>
T SinglyLinkedList<T>::removeFirst() throw(int){

	if(pFirst == NULL)
	{
		throw (int)0x1;
	}

	T node = pFirst->value;

	SinglyLinkedList<T>::Node *pNode = pFirst;
	pFirst = pNode->next;
	if(pFirst == NULL) pLast = NULL;
	delete pNode;
	return node;
}

template <typename T>
T SinglyLinkedList<T>::peekFirst() throw(int){

	if(pFirst == NULL)
	{
		throw int(0x1);
	}

	T node = pFirst->value;
	return node;
}

template <typename T>
T SinglyLinkedList<T>::removeLast() throw(int)
{
	if(pFirst == NULL)
	{
		throw (int)0x1;
	}

	T node = pLast->value;

	SinglyLinkedList<T>::Node *pNode = pFirst;

	if(pFirst == pLast)
	{
		delete pLast;
		pFirst = pLast= NULL;
		return node;
	}

	while(pNode->next != NULL && pNode->next != pLast){
		pNode=pNode->next;
	}

	delete pLast;
	pLast = pNode ;
	pNode->next = NULL;
	return node;

}

template <typename T>
std::ostream& operator<<(std::ostream& out, const SinglyLinkedList<T>& list)
{
	if(list.pFirst == NULL)
		return out;

	out<<list.pFirst->value;

	typename SinglyLinkedList<T>::Node *pNode = list.pFirst->next;

	while(pNode != NULL)
	{
		out<<" "<<pNode->value;
		pNode = pNode->next;
	}
	return out;
}

template <typename T>
SinglyLinkedList<T>& SinglyLinkedList<T>::operator=(const SinglyLinkedList<T>& list){
	SinglyLinkedList<T>::Node *pCrtNode, *pNextNode;

	pCrtNode = pFirst;
	while(pCrtNode != NULL)
	{
		pNextNode = pCrtNode->next;
		delete pCrtNode;
		pCrtNode = pNextNode;
	}
	pFirst = pLast = NULL;

	if(list.pFirst == NULL)
		return *this;


	const SinglyLinkedList<T>::Node *pListNode = list.pFirst;

	while(pListNode != NULL)
	{
		this->addLast(pListNode->value);
		pListNode = pListNode->next;
	}

	return *this;
}

template <typename T>
bool SinglyLinkedList<T>::isEmpty()
{
	if(pFirst == NULL)
		return true;

	return false;
}

template <typename T>
typename SinglyLinkedList<T>::Iterator SinglyLinkedList<T>::begin()
{
	SinglyLinkedList<T>::Iterator iterator;
	iterator.pNode = pFirst;
	iterator.pList = this;
	return iterator;
}

template <typename T>
typename SinglyLinkedList<T>::Iterator SinglyLinkedList<T>::end()
{
	SinglyLinkedList<T>::Iterator iterator;// NULL
	return iterator;
}

//------------------------------------Node-------------------------------------
template <typename T>
SinglyLinkedList<T>::Node::Node (T value) {
	this->value = value;
    next = NULL;
}

template <typename T>
SinglyLinkedList<T>::Node::Node() {
    next = NULL;
}

//------------------------------------Iterator---------------------------------
template <typename T>
SinglyLinkedList<T>::Iterator::Iterator()
{
	pNode = NULL;
	pList = NULL;
}
template <typename T>
T& SinglyLinkedList<T>::Iterator::getValue() throw(int)
{
	if(pNode == NULL)
		throw int(0x1);
	return pNode->value;
}

template <typename T>
typename SinglyLinkedList<T>::Iterator& SinglyLinkedList<T>::Iterator::operator++()
{
	pNode=pNode->next;
	return *this;
}

template <typename T>
typename SinglyLinkedList<T>::Iterator SinglyLinkedList<T>::Iterator::operator++(int)
{
	SinglyLinkedList<T>::Iterator newIterator = *this;
	pNode=pNode->next;
	return newIterator;
}

template <typename T>
bool SinglyLinkedList<T>::Iterator::addAfter(const T& value)
{
	if(pNode == NULL || pList == NULL)
		return false;

	SinglyLinkedList<T>::Node* pAddNode = new SinglyLinkedList<T>::Node(value);

	if(pNode->next == NULL)
	{
		pList->pLast = pAddNode;
	}

	pAddNode->next = pNode->next;
	pNode->next = pAddNode;

	return true;
}

template <typename T>
bool SinglyLinkedList<T>::Iterator::removeAfter()
{
	if(pNode == NULL || pList == NULL)
		return false;

	if(pNode->next == NULL)
	{
		return false;
	}

	SinglyLinkedList<T>::Node *pDelNode = pNode->next;
	pNode->next = pDelNode->next;
	delete pDelNode;

	return true;
}

template <typename T>
bool SinglyLinkedList<T>::Iterator::operator==(Iterator& it)
{
	return (pNode == it.pNode);
}

template <typename T>
bool SinglyLinkedList<T>::Iterator::operator==(Iterator it)
{
	return (pNode == it.pNode);
}

template <typename T>
bool SinglyLinkedList<T>::Iterator::operator!=(Iterator& it)
{
	return (pNode != it.pNode);
}

template <typename T>
bool SinglyLinkedList<T>::Iterator::operator!=(Iterator it)
{
	return (pNode != it.pNode);
}

template <typename T>
bool SinglyLinkedList<T>::Iterator::isValid()
{
	if(pNode != NULL)
		return true;

	return false;

}

template class SinglyLinkedList< Pair<unsigned long long,int> >;
template class SinglyLinkedList< Tranzactie >;
template class SinglyLinkedList< Pair<unsigned int,Produs> >;
template class SinglyLinkedList< Pair<unsigned long long,Tranzactie> >;
template class SinglyLinkedList< Triplet<unsigned int,unsigned int,unsigned long long> >;
template class SinglyLinkedList< Pair<unsigned int,unsigned int> >;
template class SinglyLinkedList< Triplet<unsigned int,unsigned int,unsigned int> >;
template class SinglyLinkedList< Pair<unsigned long long,unsigned int> >;
//template std::ostream& operator<<(std::ostream&, const SinglyLinkedList< Pair<long long,int> >&);
template std::ostream& operator<<(std::ostream&, const SinglyLinkedList<IdSlot>&);
template class SinglyLinkedList< Triplet<char *,unsigned int,unsigned int> >; //Task 2.3
template class SinglyLinkedList< Pair< int, Pair<Time, int> > >;
template class SinglyLinkedList< Pair <Time, int> >;
template class SinglyLinkedList<Palet>;