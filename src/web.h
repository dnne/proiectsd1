#ifndef _WEB_HEADER_
#define _WEB_HEADER_

#include <string>
#include <fstream>
#include "generic_data_structures/hash_map.h"
#include "generic_data_structures/singly_linked_list.h"
#include "Clase/Magazine.h"
#include "Clase/Tranzactie.h"
#include "Clase/Produs.h"
#include "consts.h"
#include "typedefs.h"
#include "solve.h"

const std::string index_file="build/web/index.html";
const std::string index_input_file1="src/web/1.html";
const std::string index_input_file2="src/web/2.html";
const std::string index_input_file3="src/web/3.html";
const std::string index_input_file4="src/web/4.html";
const std::string index_input_file5="src/web/5.html";
const std::string index_input_file6="src/web/6.html";
const std::string index_input_file7="src/web/7.html";

void makeWeb(Categorii& categorii,
	Magazine& magazine,
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii,
	ResizableArray< SinglyLinkedList<IdSlot> >& sloturi_pe_produse,
	ResizableArray< SinglyLinkedList<Triplet <IdPalet, IdProdus, NrItems > > > & paleti_pe_sloturi
	);

#endif