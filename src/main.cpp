#include <fstream>
#include <iostream>
#include <iomanip>

#include "load.h"
#include "hash_functions.h"
#include "solve.h"
#include "interactive.h"
#include "consts.h"
#include "typedefs.h"
#include "web.h"

int main(int argc, char const *argv[])
{
	Categorii categorii;
	Magazine magazine;
	HashMap<IdProdus,Produs> produse(NR_PRODUSE, hash_id_produs);
	HashMap<IdBon,Tranzactie> tranzactii(NR_TRANZACTII, hash_id_bon);

	//Pe pozitia i-1 gasesti sloturile pentru produsul cu id-ul i
	//voi încărca doar liste de IdSlot (unsigned int)
	ResizableArray< SinglyLinkedList<IdSlot> > sloturi_pe_produse(0);
	
	//Pe pozitia i-1 gasesti paleţii de pe slotul i
	ResizableArray < SinglyLinkedList < Triplet <IdPalet, IdProdus, NrItems > > > paleti_pe_sloturi(0);

      //ResizableArray< SinglyLinkedList< Triplet < Time, ResizableArray< produse >, id_magazin > > > &stocuri_pe_magazine
      //SinglyLinkedList<Tranzactie> sort_tranzactii(0)
      //ResizableArray < SinglyLinkedList < Pair < std::string, IdProdus > > > depozit; (=paleti_pe_sloturi)
      //

	if(loadMagazine(magazine) == false)
	{
		std::cerr<<"Error 0x1: Nu s-au putut incarca magazinele.\n";
		return 0x1;
	}
	if(loadCategorii(categorii) == false)
	{
		std::cerr<<"Error 0x2: Nu s-au putut incarca categoriile.\n";
		return 0x2;
	}
	if(loadProduse(produse) == false)
	{
		std::cerr<<"Error 0x3: Nu s-au putut incarca produsele.\n";
		return 0x3;
	}
	if(loadTranzactii(tranzactii) == false)
	{
		std::cerr<<"Error 0x4: Nu s-au putut incarca tranzactiile.\n";
		return 0x4;
	}

	if(loadPaleti(sloturi_pe_produse, paleti_pe_sloturi) == false)
	{
		std::cerr<<"Error 0x5: Nu s-au putut încărca paleţii.\n";
		return 0x5;
	}



	if(argc <= 1) // interactive mode
	{
		menuMain(categorii, magazine, produse, tranzactii, sloturi_pe_produse, paleti_pe_sloturi);
		return 0;
	}

	if(strcmp(OPTION_MAKE_WEB,argv[1]) == 0)
	{
		makeWeb(categorii, magazine, produse, tranzactii, sloturi_pe_produse, paleti_pe_sloturi);

		return 0;
	}
	return 0;
}
