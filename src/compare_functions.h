#ifndef _COMPARE_FUNCTIONS_
#define _COMPARE_FUNCTIONS_

#include "generic_data_structures/pair.h"
#include "generic_data_structures/triplet.h"
#include "typedefs.h"
#include "Clase/Time.h"
#include "Clase/Tranzactie.h"

int compare_pair_categ_sales(
	const Pair<IdCategorie,unsigned int>& p1,
	const Pair<IdCategorie,unsigned int>& p2);

int compare_triplet_prod_sales(
	const Triplet<IdProdus,IdProdus,unsigned int>& triplet1,
	const Triplet<IdProdus,IdProdus,unsigned int>& triplet2);

int compare_pair_prod_sales(
	const Pair<IdProdus,unsigned int>& p1,
	const Pair<IdProdus,unsigned int>& p2);

int compare_pair_time_counter(
        const Pair<Time,int>& p1,
        const Pair<Time,int>& p2
                              );

int compare_pair_counter_lunazi(
        const Pair<unsigned int,unsigned int>& p1,
        const Pair<unsigned int,unsigned int>& p2
                              );

int comparare_tranzactii(const Tranzactie& t1,const Tranzactie& t2);
#endif
