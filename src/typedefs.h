#ifndef _TYPEDEFS_HEADER_
#define _TYPEDEFS_HEADER_
#include <string>


typedef unsigned int IdMagazin;
typedef unsigned int IdProdus;
typedef unsigned long long IdBon;
typedef unsigned int IdCategorie;
typedef unsigned int IdSlot;
typedef unsigned int NrItems;
typedef std::string IdPalet;
typedef unsigned int minim_stoc;
typedef unsigned int IdTranzactie;
#endif