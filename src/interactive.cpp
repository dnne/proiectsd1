#include "interactive.h"
#include <iostream>
#include <iomanip>
#include <time.h>
#include "consts.h"
#include "./generic_data_structures/singly_linked_list.cpp"

// Returns continue request
bool menuDirectorExecutiv(Categorii& categorii,
	Magazine& magazine,
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii)
{

	int ans;
	std::string line;

	while(1)
	{
		std::cout<<"Sunteti autentificat ca director executiv.\n";
		std::cout<<"Selectati actiunea:\n";
		std::cout<<"1)Lista cu magazinele si vanzarile totale facute de acestea\n";
		std::cout<<"2)Lista cu produsele si vanzarile totale facute pentru acestea\n";
		std::cout<<"3)Valoarea cosului mediu\n";
		std::cout<<"4)Categoriile cele mai bine vandute pentru fiecare magazin\n";
		std::cout<<"5)Perechile de produse care se vand cel mai bine impreuna\n";
		std::cout<<"9)Reautentificare\n";
		std::cout<<"0)Exit\n";
		std::cout<<"Alegeti: ";

		getline(std::cin,line);
		try{
			ans = std::stoi(line);
		}
		catch(...)
		{
			std::cout<<"\nAlegere gresita!\n\n";
			continue;
		}

		if(ans < 0 || (ans > 5 && ans != 9) )
		{
			std::cout<<"\nAlegere gresita!\n\n";
			continue;
		}

		std::cout<<std::endl;

		if(ans == 0)
			return false;

		if(ans == 9)
			return true;

		if(ans == 1)
		{
			SinglyLinkedList< Triplet<IdMagazin,unsigned int,unsigned long long> > results;
			vanzariMagazine(results, produse, tranzactii);
			SinglyLinkedList< Triplet<IdMagazin,unsigned int,unsigned long long> >::Iterator it;
			std::cout<<std::setw(10)<<"Id Magazin";
			std::cout<<std::setw(15)<<"Locatie";
			std::cout<<std::setw(18)<<"Produse vandute";
			std::cout<<std::setw(17)<<"Vanzari totale";
			std::cout<<std::endl;
			for(it = results.begin() ; it.isValid() ; ++it)
			{
				std::cout<<std::setw(10)<<it.getValue().t1;
				std::cout<<std::setw(15)<<magazine.getMagazin(it.getValue().t1);
				std::cout<<std::setw(18)<<it.getValue().t2;
				std::cout<<std::setw(17)<<it.getValue().t3;
				std::cout<<std::endl;
			}
			std::cout<<std::endl;
			continue;
		}

		if(ans == 2)
		{
			SinglyLinkedList< Pair<IdProdus,unsigned int> > results;
			vanzariProduse(results, produse, tranzactii);
			SinglyLinkedList< Pair<IdProdus,unsigned int> >::Iterator it;
			std::cout<<std::setw(7)<<"Id Prod";
			std::cout<<std::setw(21)<<"Nume produs";
			std::cout<<std::setw(18)<<"Produse vandute";
			std::cout<<std::setw(17)<<"Vanzari totale";
			std::cout<<std::endl;
			for(it = results.begin() ; it.isValid() ; ++it)
			{
				std::cout<<std::setw(7)<<it.getValue().t1;
				std::cout<<std::setw(21)<<produse.getValue(it.getValue().t1).getName();
				std::cout<<std::setw(18)<<it.getValue().t2;
				std::cout<<std::setw(17)<<produse.getValue(it.getValue().t1).getPrice()
					*(unsigned long long)(it.getValue().t2);
				std::cout<<std::endl;
			}
			std::cout<<std::endl;
			continue;
		}

		if(ans == 3)
		{
			std::cout<<"Valoarea cosului mediu este: ";
			std::cout<<valoareCosMediu(produse,tranzactii);
			std::cout<<std::endl;
			std::cout<<std::endl;
			continue;
		}
		if(ans == 4)
		{
			SinglyLinkedList< Pair<IdCategorie,unsigned int> >::Iterator it;
			SinglyLinkedList< Pair<IdCategorie,unsigned int> > results;
			unsigned i;
			for(i = 1 ; i <= magazine.getNrElm() ; i++)
			{
				results = SinglyLinkedList< Pair<IdCategorie,unsigned int> >();
				topCategorii(results, categorii, produse, tranzactii, i);
				std::cout<<"Id magazin "<<i;
				std::cout<<" - locatie "<<magazine.getMagazin(i)<<std::endl;
				std::cout<<std::setw(9)<<"Id Categ";
				std::cout<<std::setw(20)<<"Nume categorie";
				std::cout<<std::setw(18)<<"Produse vandute";
				std::cout<<std::endl;
				for(it = results.begin() ; it.isValid() ; ++it)
				{
					if(it.getValue().t2 == 0)
						continue;
					std::cout<<std::setw(9)<<it.getValue().t1;
					std::cout<<std::setw(20)<<categorii.getCategorie(it.getValue().t1);
					std::cout<<std::setw(18)<<it.getValue().t2;
					std::cout<<std::endl;
				}
			}

			std::cout<<std::endl;
			continue;
		}
		if(ans == 5)
		{
			SinglyLinkedList< Triplet<IdProdus,IdProdus,unsigned int> > results;
			topPerechi(results, tranzactii);
			SinglyLinkedList< Triplet<IdProdus,IdProdus,unsigned int> >::Iterator it;
			std::cout<<std::setw(10)<<"Id produs1";
			std::cout<<std::setw(21)<<"Nume produs1";
			std::cout<<std::setw(11)<<"Id produs2";
			std::cout<<std::setw(21)<<"Nume produs2";
			std::cout<<std::setw(16)<<"Perechi vandute";
			std::cout<<std::endl;
			unsigned int max;
			it = results.begin();
			if(it.isValid())
				max = it.getValue().t3;
			for( ; it.isValid() ; ++it)
			{
				if(it.getValue().t3 != max)
					break;
				std::cout<<std::setw(10)<<it.getValue().t1;
				std::cout<<std::setw(21)<<produse.getValue(it.getValue().t1).getName();
				std::cout<<std::setw(11)<<it.getValue().t2;
				std::cout<<std::setw(21)<<produse.getValue(it.getValue().t2).getName();
				std::cout<<std::setw(16)<<it.getValue().t3;
				std::cout<<std::endl;
			}
			std::cout<<std::endl;
		}
	}
	return true;
}

bool menuDirectorMagazin(Magazine& magazine,
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii)
{

	int ans;
	std::string line;

	while(1)
	{
		std::cout<<"Sunteti autentificat ca director de magazin.\n";
		std::cout<<"Selectati actiunea:\n";
		std::cout<<"1)Lista cu zilele cu cele mai multe produse vandute\n";
		std::cout<<"2)Statistici cumparatori\n";
		std::cout<<"3)Afisare continut bon dupa ID\n";
		std::cout<<"4)Numarul de clienti ce ar beneficia de deschiderea unei noi case\n";
		std::cout<<"9)Reautentificare\n";
		std::cout<<"0)Exit\n";
		std::cout<<"Alegeti: ";

		getline(std::cin,line);
		try{
			ans = std::stoi(line);
		}
		catch(...)
		{
			std::cout<<"\nAlegere gresita!\n\n";
			continue;
		}

		if(ans < 0 || (ans > 4 && ans != 9) )
		{
			std::cout<<"\nAlegere gresita!\n\n";
			continue;
		}

		std::cout<<std::endl;

		if(ans == 0)
			return false;

		if(ans == 9)
			return true;

                if(ans == 1)
                        while(1)
                        {
                                typename HashMap<IdBon,Tranzactie>::Iterator it;
                                unsigned int i, k, x, max=0;;
                                unsigned int zile_saptamana[7], zile_an[366], zile_luna[31];

                                std::cout<<"Ati selectat lista cu zilele cu cele mai multe produse vandute.\n";
                                std::cout<<"Selectati actiunea:\n";
                                std::cout<<"1)Lista cu produsele vandute pe zilele saptamanii\n";
                                std::cout<<"2)Lista cu produsele vandute pe zilele anului\n";
                                std::cout<<"3)Lista cu produsele vandute pe zilele lunii\n";
                                std::cout<<"5)Lista cu zilele cu cele mai mari incasari\n";
                                std::cout<<"8)Inapoi.\n";
                                std::cout<<"9)Reautentificare\n";
                                std::cout<<"0)Exit\n";
                                std::cout<<"Alegeti:\n";
                                getline(std::cin,line);
                                try{
                                        ans = std::stoi(line);
                                }
                                catch(...)
                                {
                                        std::cout<<"\nAlegere gresita!\n\n";
                                        continue;
                                }

                                //initializare vectori
                                for (i=0; i<7; i++)
                                        zile_saptamana[i]=0;
                                for (i=0; i<366; i++)
                                        zile_an[i]=0;
                                for (i=0; i<31; i++)
                                        zile_luna[i]=0;

                                //parcurgere tranzactii pentru solutionare
                                for(it = tranzactii.begin() ; it.isValid() ; ++it)
                                {
                                        for (i = 0; i < it.getValue().produse.getNrElm(); ++i)
                                        {
                                                zile_saptamana[it.getValue().get_time().getDayoftheweek()]+=it.getValue().produse[i].t2;
                                                zile_an[it.getValue().get_time().getDayOfTheYear()]+= it.getValue().produse[i].t2;
                                                zile_luna[it.getValue().get_time().getDay()]+= it.getValue().produse[i].t2;
                                        }
                                }

                                //afisari pentru task-uri
                                if(ans == 1)
                                {
                                        std::cout<<"Luni: "<<zile_saptamana[0]<<"\n";
                                        std::cout<<"Marti: "<<zile_saptamana[1]<<"\n";
                                        std::cout<<"Miercuri: "<<zile_saptamana[2]<<"\n";
                                        std::cout<<"Joi: "<<zile_saptamana[3]<<"\n";
                                        std::cout<<"Vineri: "<<zile_saptamana[4]<<"\n";
                                        std::cout<<"Sambata: "<<zile_saptamana[5]<<"\n";
                                        std::cout<<"Duminica: "<<zile_saptamana[6]<<"\n\n";
                                }
                                if(ans == 2)
                                {
                                        x=0;
                                        while(x<10)
                                        {
                                                max=0;
                                                for (i = 0; i < 366; i++)
                                                {
                                                        if(zile_an[i] > max)
                                                        {
                                                                max=zile_an[i];
                                                                k=i;
                                                        }
                                                }
                                                std::cout<<"Ziua "<< k+1<< ":"<< max<<"\n";
                                                x++;
                                                zile_an[k]=0;
                                        }
                                        std::cout<<"\n";
                                }
                                if (ans == 3)
                                {
                                        x=0;
                                        while(x<7)
                                        {
                                                max=0;
                                                for(i=0; i<31; i++)
                                                if(zile_luna[i]>max)
                                                {
                                                        max=zile_luna[i];
                                                        k=i;
                                                }
                                                std::cout<<"Ziua "<<k+1 <<":"<<max<<"\n";
                                                x++;
                                                zile_luna[k]=0;

                                        }
                                        std::cout<<"\n";
                                }
                                if(ans==5)
                                {
                                    Pair <Time, int> pair;
                                    int key;
                                    int id_prod;
                                    int sum;
                                    HashMap<int, Pair < Time, int > > first (NR_TRANZACTII,hash_timp_unix);
	                                HashMap<IdBon,Tranzactie>::Iterator it1;
	                                SinglyLinkedList < Pair <Time, int> > incasari;
	                                SinglyLinkedList < Pair <Time, int> >::Iterator iter;
	                                for(it1 = tranzactii.begin() ; it1.isValid() ; ++it1)
	                                {
	                                    sum=0;
	                                    for (i = 0; i < it1.getValue().produse.getNrElm(); ++i)
	                                    {
	                                        id_prod=it1.getValue().produse[i].t1;
	                                        sum+=produse.getValue(id_prod).getPrice() * it1.getValue().produse[i].t2;
	                                    }    
	                                    pair.t1=it1.getValue().get_time();
	                                    pair.t2=sum;
	                                    key=(pair.t1.getYear()<<16) | pair.t1.getDayOfTheYear();
	                                    try
	                                    {
			                                first.getValue(key).t2+=sum;
		                                }	
		                                catch(int ext)
		                                {
			                                first.add(key,pair);
		                                }
	                                }
	                                HashMap<int,Pair < Time, int > >::Iterator iter1;
	                                for(iter1 = first.begin() ; iter1.isValid() ; ++iter1)
	                                {
		                                incasari.addSorted(iter1.getValue(),compare_pair_time_counter);
	                                }
	                                for(iter = incasari.begin(); iter.isValid(); ++iter)
	                                {
	                                    std::cout<<iter.getValue().t1.getDay()<<"."<<iter.getValue().t1.getMonth()<<"."<<iter.getValue().t1.getYear();
	                                    std::cout<<" -> "<<iter.getValue().t2<<"\n";
	                                }
	                                std::cout<<"\n";
                                }

                                if(ans == 8)
                                        break;
                                if(ans == 0)
                                        return false;
                                if(ans == 9)
                                        return true;
                        }
		if(ans == 2)
                {
                        //variabile intreg program
                        unsigned int i;
                        unsigned int zile_saptamana[7], luni[12];
                        Pair <unsigned int,unsigned int> zile_an[367];//t1 = tranzactii t2=luna*100+zi
                        SinglyLinkedList < Pair <Time, int> > results;
                        SinglyLinkedList < Pair <unsigned int, unsigned int> > result_all_years;
                        SinglyLinkedList < Pair <Time, int> >::Iterator fin;
                        SinglyLinkedList< Pair<unsigned int,unsigned int> >::Iterator iter2;

                        //variabile de solutionare
                        HashMap<int, Pair < Time, int > > interm(NR_TRANZACTII,hash_timp_unix);
                        HashMap<int,Pair < Time, int > >::Iterator iter1;
                        HashMap<IdBon,Tranzactie>::Iterator it;
                        Pair <Time, int> new_pair;
                        Time tranz_time;
                        int cheie;
                        //unsigned int i;

                        //initializare vectori
                        for (i=0; i<7; i++)
                                zile_saptamana[i]=0;
                        for (i=0; i<367; i++)
                                zile_an[i].t1=0;
                        for (i=0; i<12; i++)
                                luni[i]=0;

                        //parcurgere tranzactii
                        for(it = tranzactii.begin() ; it.isValid() ; ++it)
                        {
                                tranz_time=it.getValue().get_time();
                                //tranzactii pe zilele saptamanii
                                zile_saptamana[tranz_time.getDayoftheweek()]++;

                                //tranzactii pe luni din an
                                luni[tranz_time.getMonth()-1]++;

                                //tranzactii pe zile din an
                                if(tranz_time.getDay()==29&&tranz_time.getMonth()==1)
                                {
                                        zile_an[366].t1++;
                                        zile_an[366].t2=129;
                                }
                                else//29 februearie este pus pe ultima pozitie pentru a evita decalajul
                                {
                                        zile_an[tranz_time.getDayOfTheYear()].t1++;
                                        zile_an[tranz_time.getDayOfTheYear()].t2=tranz_time.getMonth()*100+tranz_time.getDay();
                                }

                                //tranzactii all time
                                new_pair.t1=tranz_time;
                                new_pair.t2=1;
                                //Adaugare in HashMap dupa
                                cheie=(new_pair.t1.getYear()<<16)|new_pair.t1.getDayOfTheYear();
                                //daca exista deja data in hashmap incrementam numarul de aparitii
                                try{
                                        interm.getValue(cheie).t2++;
                                }
                                //daca nu exista data in hashmap, o adaugam
                                catch(int ext)
                                {
                                        interm.add(cheie,new_pair);
                                }
                        }
                        //adaugare sortata intr-o lista pentru top nr_tranzactii all time
                        for(iter1 = interm.begin() ; iter1.isValid() ; ++iter1)
                        {
                                results.addSorted(iter1.getValue(),compare_pair_time_counter);
                        }
                        //adaugare intr-o lista soratata de cumparatori pe zile si luni
                        for(i=0;i<=356;i++)
                                result_all_years.addSorted(zile_an[i],compare_pair_counter_lunazi);

                        while(1)
                        {
                                int contor=10;
                                std::cout<<"Ati selectat lista cu zilele cu cele mai multe produse vandute.\n";
                                std::cout<<"Selectati actiunea:\n";
                                std::cout<<"1)Lista cu cei mai multi cumparatori - general\n";
                                std::cout<<"2)Lista cu cei mai multi cumparatori pe zilele anului\n";
                                std::cout<<"3)Lista cumparatori pe luni\n";
                                std::cout<<"4)Lista numar cumparatori pe zilele saptamanii\n";
                                std::cout<<"8)Inapoi\n";
                                std::cout<<"9)Reautentificare\n";
                                std::cout<<"0)Exit\n";
                                std::cout<<"Alegeti: ";
                                getline(std::cin,line);
                                try{
                                        ans = std::stoi(line);
                                }
                                catch(...)
                                {
                                        std::cout<<"\nAlegere gresita!\n\n";
                                        continue;
                                }
                                std::cout<<"\n";
                                if(ans == 8)
                                        break;
                                if(ans == 0)
                                        return false;
                                if(ans == 9)
                                        return true;
                                if(ans <1 || ans>4)
                                {
                                        std::cout<<"Alegere gresita!\n\n";
                                                continue;

                                }
                                if(ans == 1)
                                {
                                        contor=10;
                                        for(fin = results.begin(); fin.isValid()&&contor; ++fin)
                                        {
                                                std::cout<<std::setfill('0')<<std::setw(2)<<fin.getValue().t1.getDay()<<".";
                                                std::cout<<std::setw(2)<<fin.getValue().t1.getMonth()<<".";
                                                std::cout<<std::setw(4)<<fin.getValue().t1.getYear()<<" - ";
                                                std::cout<<std::setfill(' ')<<fin.getValue().t2<<" cumparatori\n";
                                                contor--;
                                        }
                                        std::cout<<"\n\n";
                                }

                                if(ans == 2)
                                {
                                        contor=10;
                                        for(iter2=result_all_years.begin();iter2.isValid()&&contor;++iter2)
                                        {
                                                std::cout<<std::setfill('0')<<std::setw(2)<<iter2.getValue().t2%100<<".";
                                                std::cout<<std::setw(2)<<iter2.getValue().t2/100<<" - ";
                                                std::cout<<std::setfill(' ')<<iter2.getValue().t1<<" cumparatori\n";
                                                contor--;
                                        }
                                        std::cout<<"\n\n";
                                }
                                if (ans == 3)
                                {
                                        std::cout<<std::setw(12)<<"Ianuarie: "<<luni[0]<<" cumparatori\n";
                                        std::cout<<std::setw(12)<<"Februarie: "<<luni[1]<<" cumparatori\n";
                                        std::cout<<std::setw(12)<<"Martie: "<<luni[2]<<" cumparatori\n";
                                        std::cout<<std::setw(12)<<"Aprilie: "<<luni[3]<<" cumparatori\n";
                                        std::cout<<std::setw(12)<<"Mai: "<<luni[4]<<" cumparatori\n";
                                        std::cout<<std::setw(12)<<"Iunie: "<<luni[5]<<" cumparatori\n";
                                        std::cout<<std::setw(12)<<"Iulie: "<<luni[6]<<" cumparatori\n";
                                        std::cout<<std::setw(12)<<"August: "<<luni[7]<<" cumparatori\n";
                                        std::cout<<std::setw(12)<<"Septembrie: "<<luni[8]<<" cumparatori\n";
                                        std::cout<<std::setw(12)<<"Octombrie: "<<luni[9]<<" cumparatori\n";
                                        std::cout<<std::setw(12)<<"Noiembrie: "<<luni[10]<<" cumparatori\n";
                                        std::cout<<std::setw(12)<<"Decembrie: "<<luni[11]<<" cumparatori\n\n";
                                }
                                if(ans == 4)
                                {
                                        std::cout<<std::setw(10)<<"Luni: "<<zile_saptamana[0]<<" cumparatori\n";
                                        std::cout<<std::setw(10)<<"Marti: "<<zile_saptamana[1]<<" cumparatori\n";
                                        std::cout<<std::setw(10)<<"Miercuri: "<<zile_saptamana[2]<<" cumparatori\n";
                                        std::cout<<std::setw(10)<<"Joi: "<<zile_saptamana[3]<<" cumparatori\n";
                                        std::cout<<std::setw(10)<<"Vineri: "<<zile_saptamana[4]<<" cumparatori\n";
                                        std::cout<<std::setw(10)<<"Sambata: "<<zile_saptamana[5]<<" cumparatori\n";
                                        std::cout<<std::setw(10)<<"Duminica: "<<zile_saptamana[6]<<" cumparatori\n\n";
                                }
                        }
                }
		if(ans == 3)
		{
			SinglyLinkedList< Triplet<char *,unsigned int,unsigned int> > results;
			SinglyLinkedList< Triplet<char *,unsigned int,unsigned int> >::Iterator it;
			IdBon bon_id;
			Tranzactie tranz_curent;
			unsigned int pret_total=0,afis=28;
			std::cout<<"Introduceti ID-ul bonului cautat:\n";
			getline(std::cin,line);
			std::cout<<"\n\n";
			bon_id=std::stoull(line);

			//easter egg Terminator
			if(bon_id==1991)
			{
				std::cout<<std::setw(20)<<"BON FISCAL\n";
				std::cout<<std::setw(26)<<"S.C. SportsRUs S.R.L\n";
				std::cout<<std::setw(13)<<"Sucursala ";
				std::cout<<std::setw(15)<<magazine.getMagazin(tranz_curent.get_id_magazin())<<"\n\n";
				std::cout<<std::setw(11)<<"ID bon: ";
				std::cout<<std::setw(16)<<bon_id<<"\n";
				std::cout<<std::setw(30)<<"BINE ATI VENIT LA SPORTSRUS\n\n";
				std::cout<<"  "<<"Clothes"<<"\n";
				std::cout<<std::setw(7)<<"800";
				std::cout<<" x"<<"1";
				std::cout<<std::setw(17)<<"800"<<"\n ";
				std::cout<<"  "<<"Boots"<<"\n";
				std::cout<<std::setw(7)<<"1000";
				std::cout<<" x"<<"1";
				std::cout<<std::setw(17)<<"1000"<<"\n ";
				std::cout<<"  "<<"Motorcycle"<<"\n";
				std::cout<<std::setw(7)<<"2007";
				std::cout<<" x"<<"1";
				std::cout<<std::setw(17)<<"2007"<<"\n ";
				while(afis--)
						std::cout<<"-";
				std::cout<<"\n";
				std::cout<<std::setw(8)<<"TOTAL";
				std::cout<<std::setw(19)<<"3807"<<"\n";
				std::cout<<std::setw(16)<<"Va multumim!\n"<<'\n';
				std::cout<<"Data: "<<"3"<<'/';
				std::cout<<"6"<<'/';
				std::cout<<"1991"<<'\n';
				std::cout<<"Ora: "<<"20"<<':';
				std::cout<<"16"<<':';
				std::cout<<"10\n"<<std::endl;

			}

			else
			{
				try{
				        //incerrcam sa extragem tranzactia cu cheia intorodusa
					tranz_curent=tranzactii.getValue(bon_id);
				}
				catch(...){
					//daca nu exista, inseamna ca a fost introdus un IdBon invalid
					std::cout<<"ID-ul bonului cautat nu exista in baza de date!\n\n";
					continue;
				}

				//Rand de 30 de caractere
				std::cout<<std::setw(20)<<"BON FISCAL\n";
				std::cout<<std::setw(26)<<"S.C. SportsRUs S.R.L\n";
				std::cout<<std::setw(13)<<"Sucursala ";
				std::cout<<std::setw(15)<<magazine.getMagazin(tranz_curent.get_id_magazin())<<"\n\n";
				std::cout<<std::setw(11)<<"ID bon: ";
				std::cout<<std::setw(16)<<bon_id<<"\n";
				std::cout<<std::setw(30)<<"BINE ATI VENIT LA SPORTSRUS\n\n";

				//task solver
				obtineBon(tranz_curent,produse,results);

				//parcurgem "lista" de produse de pe bon
				for(it=results.begin() ; it.isValid() ; ++it )
				{
					pret_total+=it.getValue().t3;

					std::cout<<"  "<<it.getValue().t1<<"\n";
					std::cout<<std::setw(7)<<it.getValue().t3;
					std::cout<<" x"<<it.getValue().t2;
					std::cout<<std::setw(17)<<it.getValue().t2*it.getValue().t3<<"\n ";
				}
				while(afis--)
						std::cout<<"-";
				std::cout<<"\n";
				std::cout<<std::setw(8)<<"TOTAL";
				std::cout<<std::setw(19)<<pret_total<<"\n";
				std::cout<<std::setw(16)<<"Va multumim!\n"<<'\n';
				std::cout<<" Data: "<<tranz_curent.get_time().getDay()<<'/';
				std::cout<<tranz_curent.get_time().getMonth()<<'/';
				std::cout<<tranz_curent.get_time().getYear()<<'\n';
				std::cout<<" Ora: "<<tranz_curent.get_time().getHour()<<':';
				std::cout<<tranz_curent.get_time().getMin()<<':';
				std::cout<<tranz_curent.get_time().getSec()<<"\n"<<std::endl;
			}
		}
		if(ans==4)
		{
		        //beneficiari este un vector in care numaram cati oameni ar beneficia de o noua casa
			int * beneficiari;
			unsigned int i;
			beneficiari=new int[magazine.getNrElm()];

			//task solver
			catiClienti(magazine,tranzactii,beneficiari);

			std::cout<<std::setw(11)<<"Magazin";
			std::cout<<std::setw(15)<<"Beneficiaza\n";
			for(i=0;i<magazine.getNrElm();i++)
			{
				std::cout<<std::setw(11)<<magazine.getMagazin(i+1);
				if(beneficiari[i]==0)
                                {
                                        std::cout<<std::setw(17)<<"Niciun client\n";
                                        continue;
                                }
				std::cout<<std::setw(5)<<beneficiari[i];
				std::cout<<" - client";
				if(beneficiari[i]>1)
                                        std::cout<<"i";
                                std::cout<<"\n";
			}
			std::cout<<std::endl;
			delete[] beneficiari;
		}
	}
}

bool menuSefDepozit(ResizableArray< SinglyLinkedList<IdSlot>> &sloturi_pe_produse,
					ResizableArray< SinglyLinkedList<Triplet <IdPalet, IdProdus, NrItems > > > &paleti_pe_sloturi,
					Magazine& magazine,
					HashMap<IdBon,Tranzactie>& tranzactii,
					HashMap<IdProdus,Produs>& produse
					)
{

	int ans;
	unsigned int id_prod;
	std::string line;

	while(1)
	{
		std::cout<<"Sunteti autentificat ca sef depozit.\n";
		std::cout<<"Selectati actiunea:\n";
		std::cout<<"1)Afiseaza sloturile in care se gaseste un anumit tip de produs\n";
		std::cout<<"2)Afiseaza ce paleti trebuie mutati pentru a obtine un palet cu un anumit produs\n";
		std::cout<<"3)Afiseaza prima comanda pe care nu o poate onora depozitul\n";
		std::cout<<"9)Reautentificare\n";
		std::cout<<"0)Exit\n";
		std::cout<<"Alegeti: ";

		getline(std::cin,line);
		try{
			ans = std::stoi(line);
		}
		catch(...)
		{
			std::cout<<"\nAlegere gresita!\n\n";
			continue;
		}

		if(ans < 0 || (ans > 3 && ans != 9) )
		{
			std::cout<<"\nAlegere gresita!\n\n";
			continue;
		}

		std::cout<<std::endl;

		if(ans == 0)
			return false;

		if(ans == 9)
			return true;
		if(ans == 1)
		{
			std::cout<<"Introduceti id-ul produsului:\n";
			getline(std::cin,line);
			std::cout<<"\n\n";

			try
			{
				id_prod = std::stoi(line);
			}
			catch(...)
			{
				// Ar fi bine ca aici să exite posibilitatea sa reintroduca id-ul, fara sa aleaga mai intai 1!
				std::cout<<"ID-ul produsului cautat nu exista in depozitul dumneavoastra!\n\n";
				continue;
			}

			if(id_prod > 0 && id_prod <= sloturi_pe_produse.getNrElm())
			{
				std::cout<<"Produsul cu al carui id este " << id_prod << " se gaseste pe urmatoarele sloturi:\n";
				std::cout<<sloturi_pe_produse[id_prod-1];
				std::cout<<"\n\n\n";
			}
			else
			{
				// Ar fi bine ca aici să exite posibilitatea sa reintroduca id-ul, fara sa aleaga mai intai 1!
				std::cout<<"ID-ul produsului cautat nu exista in depozitul dumneavoastra!\n\n\n";
			}
		}

		if(ans == 2)
		{
			std::cout<<"Introduceti id-ul produsului:\n";
			getline(std::cin,line);
			std::cout<<"\n\n";

			try
			{
				id_prod = std::stoi(line);
			}
			catch(...)
			{
				// Ar fi bine ca aici să exite posibilitatea sa reintroduca id-ul, fara sa aleaga mai intai 1!
				std::cout<<"ID-ul produsului cautat nu exista in depozitul dumneavoastra!\n\n";
				continue;
			}

			int b = 1;

			if(id_prod > 0 && id_prod <= sloturi_pe_produse.getNrElm()){
				std::cout<<"Produsul al carui id este " << id_prod;
				int id_slot = sloturi_pe_produse[id_prod-1].peekFirst();
				if(paleti_pe_sloturi[id_slot-1].peekFirst().t2 == id_prod){
					std::cout<<" este primul din slotul "<<id_slot<<"."<<std::endl;
					std::cout<<"Nu este nevoie de nicio mutare.";
					std::cout<<"\n\n";
					b = 0;
				}

				if(b == 1){
					std::cout<<" se gaseste cel mai sus pe slotul "<<id_slot;
				std::cout << " si s-a obtinut prin mutarea paletilor cu urmatoarele id-uri:\n";

				SinglyLinkedList< Triplet <IdPalet, IdProdus, NrItems > >::Iterator it;

				for( it = paleti_pe_sloturi[id_slot-1].begin(); it.isValid() ; ++it ) {
					if(it.getValue().t2 != id_prod) {
						std::cout<<it.getValue().t1;
						if(it.getValue().t2+1 == paleti_pe_sloturi.getNrElm())
							std::cout<<" pe slotul 1 "<<std::endl;
						else
							std::cout<<" pe slotul "<<id_slot+1<<std::endl;
					}
					else
						break;
				}
				std::cout<<"Mutam inapoi pe slotul "<<id_slot;
				std::cout<<" paletii de mai sus in ordinea inversa scoaterii lor.\n";
				std::cout<<"\n\n\n";
				}
			}
			else{
				// Ar fi bine ca aici să exite posibilitatea sa reintroduca id-ul, fara sa aleaga mai intai 1!
				std::cout<<"ID-ul produsului cautat nu exista in depozitul dumneavoastra!\n\n\n";
			}
		}

		if(ans == 3)
		{
			SinglyLinkedList<Tranzactie> sort_tranzactii;

			HashMap<IdBon,Tranzactie>::Iterator it;

			for(it=tranzactii.begin();it.isValid();++it)
			{
				sort_tranzactii.addSorted(it.getValue(),comparare_tranzactii);
			}

			ResizableArray< ResizableArray< Pair< unsigned int , minim_stoc > > > stocuri_pe_magazine(0);

			stocuri_pe_magazine.setNrElm(magazine.getNrElm());

			for (unsigned int i=0;i<magazine.getNrElm();i++)	//initializare stocuri_pe_magazine
			{
				stocuri_pe_magazine[i].setNrElm(sloturi_pe_produse.getNrElm());
			}

			ResizableArray < SinglyLinkedList < Triplet <IdPalet, IdProdus, NrItems > > > depozit_sloturi(0);

			depozit_sloturi.setNrElm(paleti_pe_sloturi.getNrElm());
			// de aici luam paleti
			depozit_sloturi = paleti_pe_sloturi;	// copie paleti (adica stiva pe sloturi)

			ResizableArray< SinglyLinkedList<IdSlot > > depozit_produse(0);

			depozit_produse.setNrElm(sloturi_pe_produse.getNrElm());
			// produsele in ordinea id-ului
			depozit_produse = sloturi_pe_produse;	// copie produse (sloturile unde se gaseste produsul i)

			bool gata = true;

			for(unsigned int iteratie = 0 ; iteratie < magazine.getNrElm(); iteratie++)	//pentru fiecare magazin
			{
				for (unsigned int jteratie = 0 ; jteratie < depozit_produse.getNrElm(); jteratie++)	//pentru fiecare produs
				{
					int x = depozit_produse[jteratie].removeFirst();	//primul palet unde se gaseste produsul jteratie

					SinglyLinkedList < Triplet <IdPalet, IdProdus, NrItems > > ::Iterator it;
					SinglyLinkedList < Triplet <IdPalet, IdProdus, NrItems > > ::Iterator sterg;

					Triplet <IdPalet, IdProdus, NrItems > y;
					//iau cel mai de sus palet de pe slotul x
					try{
						y = depozit_sloturi[x-1].peekFirst();
					}
					catch(...)
					{
						gata = false;
						break;
					}

					//si verific daca primul palet este paletul cu produsul jteratie
					if(y.t2 == jteratie+1){

						stocuri_pe_magazine[iteratie][jteratie].t1 = y.t3;
						stocuri_pe_magazine[iteratie][jteratie].t2 = y.t3 / 10;
						depozit_sloturi[x-1].removeFirst();

					}
					//daca nu, parcurg lista de paleti pana dau de el
					else{
						gata = false;
						for(it = depozit_sloturi[x-1].begin(); it.isValid(); ++it)
						{

							if(it.getValue().t2 == jteratie+1)
							{
								stocuri_pe_magazine[iteratie][jteratie].t1 = it.getValue().t3;
								stocuri_pe_magazine[iteratie][jteratie].t2 = it.getValue().t3 / 10;
								sterg.removeAfter();
								gata = true;
								break;
							}
							sterg=it;
						}
						if(gata == false)
						{
							break;
						}
					}
				}
				if(gata == false)
				{
					break;
				}
			}

			if(gata == false)
			{
				std::cout<<"Depozitul nu poate sa imparta paletii initiali\n";
				continue;
			}

			SinglyLinkedList<Tranzactie>::Iterator i;

			for(i=sort_tranzactii.begin();i.isValid();++i)	//parcurg tranzactiile
			{
				//pentru fiecare tranzactie
				unsigned int mag = i.getValue().get_id_magazin();
				//pentru fiecare produs
				for (unsigned int j=0; j < i.getValue().produse.getNrElm(); j++)
				{
					//imi iau tip produs si nr elemente de tip produs
					unsigned int tip_prod = i.getValue().produse[j].t1;
					unsigned int nr_prod = i.getValue().produse[j].t2;
					// daca am in stoc, ii dau (adica le scad din stoc)
					if( nr_prod < stocuri_pe_magazine[mag-1][tip_prod-1].t1 )
					{
						stocuri_pe_magazine[mag-1][tip_prod-1].t1 -= nr_prod;
						//daca am ajuns sub 10%
						if( stocuri_pe_magazine[mag-1][tip_prod-1].t1 <= stocuri_pe_magazine[mag-1][tip_prod-1].t2 )
						{
							//daca nu mai am in depozit, nu mai pot onora realimentarea stocului
							if(depozit_produse[tip_prod-1].isEmpty())
							{
								std::cout<<"Prima comanda pe care depozitul nu o mai poate onora ";
								std::cout<<"este in tranzactia cu id-ul "<<i.getValue().getIdTranzactie();
								std::cout<<" realizata in sucursala din "<<magazine.getMagazin(mag)<<",depozitul ramanand fara "<<produse.getValue(tip_prod).getName()<<"."<<"\n \n \n";
								gata = false;
								break;
							}
							//daca mai am in depozit, alimentez stocul
							int x = depozit_produse[tip_prod-1].removeFirst();

							SinglyLinkedList < Triplet <IdPalet, IdProdus, NrItems > > ::Iterator it;
							SinglyLinkedList < Triplet <IdPalet, IdProdus, NrItems > > ::Iterator sterg;

							Triplet <IdPalet, IdProdus, NrItems > y;

							y = depozit_sloturi[x-1].peekFirst();

							if(y.t2 == tip_prod)
							{
								stocuri_pe_magazine[mag-1][tip_prod-1].t1 = y.t3;
								stocuri_pe_magazine[mag-1][tip_prod-1].t2 = y.t3 / 10;
								depozit_sloturi[x-1].removeFirst();
							}
							else
							{
								for(it = depozit_sloturi[x-1].begin(); it.isValid(); ++it)
								{
									if(it.getValue().t2 == tip_prod)
									{
										stocuri_pe_magazine[mag-1][tip_prod-1].t1 = it.getValue().t3;
										stocuri_pe_magazine[mag-1][tip_prod-1].t2 = it.getValue().t3 / 10;
										sterg.removeAfter();
										break;
									}
									sterg = it;
								}
							}
						}
					}
					//daca tranzactia cere mai multe produse decat stocul curent al magazinului
					if( nr_prod > stocuri_pe_magazine[mag-1][tip_prod-1].t1 )
					{
					//verific daca mai am in depozit ca sa realimentez stocul
						if(depozit_produse[tip_prod-1].isEmpty())
						{
							std::cout<<"Depozitul nu mai poate dispune de produsul de tip "<<tip_prod<<" "<<"pentru magazinul "<<mag<<"\n \n \n";
							gata = false;
							break;
						}
						else
						{
							int x = depozit_produse[tip_prod-1].removeFirst();
							SinglyLinkedList < Triplet <IdPalet, IdProdus, NrItems > > ::Iterator it;
							SinglyLinkedList < Triplet <IdPalet, IdProdus, NrItems > > ::Iterator sterg;

							Triplet <IdPalet, IdProdus, NrItems > y;
							y = depozit_sloturi[x-1].peekFirst();
							if(y.t2 == tip_prod)
							{
								stocuri_pe_magazine[mag-1][tip_prod-1].t1 = y.t3;
								stocuri_pe_magazine[mag-1][tip_prod-1].t2 = y.t3 / 10;
								depozit_sloturi[x-1].removeFirst();
							}
							else
							{
								for(it = depozit_sloturi[x-1].begin(); it.isValid(); ++it)
								{
									if(it.getValue().t2 == tip_prod)
									{
										stocuri_pe_magazine[mag-1][tip_prod-1].t1 = it.getValue().t3;
										stocuri_pe_magazine[mag-1][tip_prod-1].t2 = it.getValue().t3 / 10;
										sterg.removeAfter();
									}
									sterg = it;
								}
							}
						}
						stocuri_pe_magazine[mag-1][tip_prod-1].t1 -= nr_prod;
					}
				}

				if( gata == false )
				{
					break;
				}
			}
		}
	}

}

void menuMain(Categorii& categorii,
	Magazine& magazine,
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii,
	ResizableArray< SinglyLinkedList<IdSlot> >& sloturi_pe_produse,
	ResizableArray< SinglyLinkedList<Triplet <IdPalet, IdProdus, NrItems > > > & paleti_pe_sloturi
	)
{

	int ans;
	std::string line;

	while(1)
	{
		std::cout<<"Autentificati-va ca:\n";
		std::cout<<"1)Director executiv\n";
		std::cout<<"2)Director de magazin\n";
		std::cout<<"3)Sef de depozit\n";
		std::cout<<"0)Exit\n";
		std::cout<<"Alegeti: ";

		getline(std::cin,line);
		try{
			ans = std::stoi(line);
		}
		catch(...)
		{
			std::cout<<"\nI'm sorry, Dave. I'm afraid I can't do that.\nAlegere gresita!\n\n";
			continue;
		}

		if((ans < 0 || ans > 3) && ans!=42)
		{
			std::cout<<"\nI'm sorry, Dave. I'm afraid I can't do that.\nAlegere gresita!\n\n";
			continue;
		}

		std::cout<<std::endl;

		if(ans == 0)
			return;

		if(ans == 1)
		{
			if(menuDirectorExecutiv(categorii, magazine, produse, tranzactii) == false)
				return;
			continue;
		}
		if(ans == 2)
		{
			if(menuDirectorMagazin(magazine, produse, tranzactii) == false)
				return;
			continue;
		}
		if(ans == 3)
		{
			if(menuSefDepozit(sloturi_pe_produse, paleti_pe_sloturi,magazine,tranzactii,produse) == false)
				return;
			continue;
		}
		if(ans == 42)
		{
			std::cout<<"\nIn the beginning the universe was created. This made a lot of people angry and has widely been considered as a bad move.\n\n";
			continue;
		}

	}
};
