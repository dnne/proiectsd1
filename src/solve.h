#ifndef _SOLVE_HEADER_
#define _SOLVE_HEADER_

#include "Clase/Produs.h"
#include "Clase/Tranzactie.h"
#include "Clase/Categorii.h"
#include "Clase/Magazine.h"
#include "generic_data_structures/singly_linked_list.h"
#include "generic_data_structures/hash_map.h"
#include "generic_data_structures/Stack.h"
#include "generic_data_structures/triplet.h"
#include "compare_functions.h"
#include "hash_functions.h"
#include "consts.h"
#include "typedefs.h"

/* Functia ce rezolva primul task de la directorii executivi.
   Rezultatul functiei este intors sub forma unei liste(results) de tripleti
   (id magazin, nr produse vandute, suma totala incasata din vanzari), sortata
   dupa id-ul magazinului
 */
void vanzariMagazine(
	SinglyLinkedList< Triplet<IdMagazin,unsigned int,unsigned long long> >& results,
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii);


/* Rezolvarea task-ului 2 de la directorii executivi
   Rezultatul este intros sub forma unei liste(results) de perechi
   (id produs, numarul de produse de acest tip vandute),
   sortata dupa id-ul produsului
*/
void vanzariProduse(
   SinglyLinkedList< Pair<IdProdus,unsigned int> >& results,
   HashMap<IdProdus,Produs>& produse,
   HashMap<IdBon,Tranzactie>& tranzactii);

/* Rezolvarea taskului 3 de la directorii executivi
	Rezultatul este intors sub forma de numar real (double)
*/
double valoareCosMediu(
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii);

/* Rezolvarea task-ului 4 de la directorii executivi, pentru un anumit magazin
   transmis ca parametru (id_magazin).
   Rezultatul este stocat sub forma unei liste(results) de perechi
   (categorie, numar de produse din categorie vandute), sortata dupa numarul de
   produse vandute.
*/
void topCategorii(
	SinglyLinkedList< Pair<IdCategorie,unsigned int> >& results,
	Categorii& categorii,
	HashMap<IdProdus,Produs>& produse,
	HashMap<IdBon,Tranzactie>& tranzactii,
	IdMagazin id_magazin);

/* Rezolvarea taskului 5 de la directorii executivi
   Rezultatul este transmis sub forma unei liste(results) de tripleti
   (id produs 1 , id produs 2 , numarul de perechi)
*/
void topPerechi(
	SinglyLinkedList< Triplet<IdProdus,IdProdus,unsigned int> >& results,
	HashMap<IdBon,Tranzactie>& tranzactii);

/* Rezolvarea taskului 3 de la directorii magazine
   Rezultatul este transmis sub forma unei liste(results) de tripleti
   (nume produs , cantitate produs , pret produs)
*/

void obtineBon(Tranzactie& tranz_curent,
               HashMap<IdProdus,Produs>& produse,
               SinglyLinkedList< Triplet<char *,unsigned int,unsigned int> >& results);

/* Rezolvarea taskului 4 de la directorii magazine
   Rezultatul este transmis sub forma unui vector de tipul
   (results[id_magazin-1]=cati_clieti_beneficiaza)
*/
void catiClienti( Magazine& magazine,
                  HashMap<IdBon,Tranzactie>& tranzactii,
                  int beneficiari[]);

#endif
