#ifndef _CONSTS_HEADER_
#define _CONSTS_HEADER_

#define NR_PRODUSE 50
#define NR_TRANZACTII 5000
#define SIZE_RA 10
#define NUMBER_OF_PRODUCTS 57
#define NUMBER_OF_SLOTS 30
#define OPTION_MAKE_WEB "--makeweb"
#define TIME_PER_PROD 15
#define TIME_PER_TRANZ 90

#endif