#include "load.h"

#include <fstream>

bool isMagazineLoaded = false;
bool isTranzactiiLoaded = false;
bool isBonuriLoaded = false;
bool isProduseLoaded = false;
bool isCategoriiLoaded = false;
bool isPaletiLoaded = false;

bool loadMagazine(Magazine& magazine)
{
	std::ifstream file(magazine_csv.c_str());
	if(!file) // Eroare deschidere
		return false;

	std::string line,data;
	std::size_t crt_token = 0;

	getline(file, line); // cap de tabel
	while(getline(file, line))
	{
		crt_token = line.find(file_token);
		data= line.substr(crt_token + 1, line.length()-crt_token-1);

		magazine.addMagazin(data);
	}


	file.close();
	isMagazineLoaded= true;
	return true;
}

bool loadCategorii(Categorii& categorii)
{

	std::ifstream file(categorii_csv.c_str());
	if(!file) // Eroare deschidere
		return false;

	std::string line,data;
	std::size_t crt_token = 0;

	getline(file, line); // cap de tabel
	while(getline(file, line))
	{
		crt_token = line.find(file_token);
		data= line.substr(crt_token + 1, line.length()-crt_token-1);

		categorii.addCategorie(data);
	}


	file.close();
	isCategoriiLoaded= true;
	return true;
}

bool loadProduse(HashMap<IdProdus,Produs>& produse)
{

	std::ifstream file(produse_csv.c_str());
	if(!file) // Eroare deschidere
		return false;

	std::string line, data;
	std::size_t last_token, crt_token;

	Produs crt_produs;
	IdProdus id;

	getline(file, line); // cap de tabel
	while(getline(file, line))
	{
		last_token = 0;

		try
		{
			// Id
			crt_token = line.find(file_token);
			data = line.substr(last_token, crt_token - last_token);
			id = std::stoi(data);

			// Name
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, crt_token - last_token);
			crt_produs.Set_nume(data.c_str());

			// Categ
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, crt_token - last_token);
			crt_produs.Set_cat_id(std::stoi(data));

			//Price
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, line.length() - last_token);
			crt_produs.Set_pret(std::stoi(data));

			produse.add(id,crt_produs);
		}
		catch(...)
		{
			continue;
		}
	}

	file.close();
	isProduseLoaded = true;
	return true;
}

bool loadTranzactii(HashMap<IdBon,Tranzactie>& tranzactii)
{
	std::ifstream tranzactii_file(tranzactii_csv.c_str());
	if(!tranzactii_file) // Eroare deschidere
		return false;

	std::ifstream bonuri_file(bonuri_csv.c_str());
	if(!tranzactii_file) // Eroare deschidere
	{
		tranzactii_file.close();
		return false;
	}

	std::string line, data;
	std::size_t last_token, crt_token;

	Tranzactie crt_tranzactie;
	IdBon id;

	getline(tranzactii_file, line); // cap de tabel
	while(getline(tranzactii_file, line))
	{
		last_token = 0;

		try
		{
			// id tranzactie
			crt_token = line.find(file_token);
			data = line.substr(last_token, crt_token - last_token);
			crt_tranzactie.setIdTranzactie(std::stoi(data));

			// timestamp
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, crt_token - last_token);
			crt_tranzactie.setTime(data.c_str());

			// id_bon
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, crt_token - last_token);
			id=std::stoull(data);

			// id_magazin
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, line.length() - last_token);
			crt_tranzactie.setIdMagazin(std::stoi(data));

			tranzactii.add(id,crt_tranzactie);
		}
		catch(...)
		{
			continue;
		}
	}

	getline(bonuri_file, line); // cap de tabel
	while(getline(bonuri_file, line))
	{
		last_token = 0;
		try
		{
			// id bon
			crt_token = line.find(file_token);
			data = line.substr(last_token, crt_token - last_token);
			id = std::stoull(data);

			// id_produs
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, line.length() - last_token);
			tranzactii.getValue(id).addProdus(std::stoi(data));
		}
		catch(...)
		{
			continue;
		}
	}

	tranzactii_file.close();
	isTranzactiiLoaded = true;
	return true;
}

/*
sloturi_pe_produse --- pe pozitia i-1 gasesti sloturile pentru produsul cu id-ul i
paleti_pe_sloturi  --- pe pozitia i-1 gasesti paleţii de pe slotul i
*/
bool loadPaleti(ResizableArray< SinglyLinkedList<IdSlot> > & sloturi_pe_produse,
				ResizableArray< SinglyLinkedList< Triplet <IdPalet, IdProdus, NrItems > > > & paleti_pe_sloturi)
{

	std::ifstream file(paleti_csv.c_str());
	if(!file) // Eroare deschidere
	{
		std::cerr<<"Nu am putut deschide fişierul paleti.csv\n";
		return false;
	}

	Triplet <IdPalet, IdProdus, NrItems > p;
	std::string line, data;
	std::size_t last_token, crt_token;

	Palet crt_palet;

	getline(file, line); // capul de tabel pe care îl ignor
	while(getline(file, line))//cât timp am ce citi
	{
		last_token = 0;

		try
		{	//Introduc informaţiile în variabila de tip Palet

			// Id palet
			crt_token = line.find(file_token);
			data = line.substr(last_token, crt_token - last_token);
			crt_palet.setId(data);

			// prod_type
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, crt_token - last_token);
			crt_palet.setprod_type(std::stoi(data));

			// n_items
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, crt_token - last_token);
			crt_palet.setn_items(std::stoi(data));

			//slot
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, line.length() - last_token);
			crt_palet.setslot(std::stoi(data));
			

			//Verific dacă vectorul e îndeajuns de mare 

			if (crt_palet.getProd_type() > sloturi_pe_produse.getCapacity())
			{

				sloturi_pe_produse.setNrElm(crt_palet.getProd_type());
			}

			//Adaug numarul slotului in lista de pe pozitia prod_type
			//Pe poziţia 0 voi gasi sloturile în care gasesc produsul cu id-ul 1.
			sloturi_pe_produse[crt_palet.getProd_type() - 1].addFirst(crt_palet.getSlot());

			//std::cout<<sloturi_pe_produse.getCapacity()<<std::endl;

			if (crt_palet.getSlot() > paleti_pe_sloturi.getCapacity())
			{
				//std::cout<<crt_palet.getSlot();
				//std::cout<<"if load\n";
				paleti_pe_sloturi.setNrElm(crt_palet.getSlot());
			}
			//std::cout<<"outside if load\n";
			p.t1 = crt_palet.getId();
			p.t2 = crt_palet.getProd_type();
			p.t3  = crt_palet.getN_items();
			paleti_pe_sloturi[crt_palet.getSlot() - 1].addFirst(p);
		}
		catch(...)
		{
			continue;
		}
	}

	file.close();
	isPaletiLoaded = true;
	return true;
}
