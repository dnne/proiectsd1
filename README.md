# **Software aplication for SportRUs**  

  
## **Build** and **run**

1.`git clone https://gitlab.cs.pub.ro/cristian.done/proiectsd1.git`   
2.`cd proiectsd1`   
3.`make`  
4.`./main --makeWeb` for HTML results or   
5.`./main` for CLI  
  
  
  
## **Team 311CAa**
+ Cristian Done 
+ Cristian Duţescu
+ Radu Nicolau
+ Daniel Dosaru
+ Daniel Bălteanu
+ Cristina Buciu
+ Camelia Moise
+ Claudiu Nedelcu
+ Irina Băeţica
+ Sergiu Isopescu
+ Teodora Chiriac
+ Radu Ciunghia
+ Antonia Nicolaescu
