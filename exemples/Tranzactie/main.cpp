#include "../../src/Clase/Tranzactie.h"
#include <iostream>


using namespace std;

int main() {

	Tranzactie a;
	a.setId(10);
	a.setIdMagazin(2);
	Pair<Produs*,unsigned int> x;
	x.t2 = 5;
	x.t1 = NULL;
	a.produse.add(x);
	Pair<Produs*,unsigned int> y;
	y.t2 = 10;
	y.t1 = NULL;
	a.produse.add(y);
	int b = a.produse.getNrElm();
	cout<<b<<endl;

	//Testing setTime method
	char timestamp[25];
	cout<<"Give me the timestamp from tranzactii.csv:"<<endl;
	fgets(timestamp, 25, stdin);
	a.setTime(timestamp);

	return 0;
}
