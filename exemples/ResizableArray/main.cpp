#include <iostream>
#include "../../src/generic_data_structures/resizable.h"


int main(){

	int x;
	bool b;
	unsigned n, i;
	ResizableArray<int> ra,ra2;
	std::cout<<"Nr adaugare elemente:";
	std::cin>>n;
	for(i= 1; i <= n ; i++)
	{
		std::cout<<"adaugare element nr "<<i<<":";
		std::cin>>x;
		ra.add(x);
		std::cout<<"Dimensiune dupa actualizare:"<<ra.getCapacity()<<"\n Elemente:\n"<<ra;
	}

	std::cout<<"Testare operator []:\n";
	try{
		std::cout<<"ra["<<0<<"]="<<ra[0]<<std::endl;
		std::cout<<"ra[ra.NrElm()]=ra["<<ra.getNrElm()<<"]=ra[0]="<<ra[ra.getNrElm()]<<std::endl;
		std::cout<<"ra["<<-1<<"]=ra[ra.getNrElm()-1]="<<ra[-1]<<std::endl;
	}
	catch(int error_code){
		std::cout<<"Error "<<error_code<<" - ra gol";
	}

	std::cout<<"Nr stergere elemente:";
	std::cin>>n;
	for(i= 1; i <= n ; i++)
	{
		std::cout<<"stergere element nr "<<i<<":";
		std::cin>>x;
		b = ra.remove_element(x);
		if(b == 1) std::cout<<"Operatie executata cu succes\n";
		std::cout<<"Dimensiune dupa actualizare:"<<ra.getCapacity()<<"\n Elemente:\n"<<ra;
	}

	std::cout<<"Copiere ra\n";
	ra2 = ra;
	ResizableArray<int> ra3 = ra;

	std::cout<<"ra2:"<<ra2<<"ra3:"<<ra3;

	return 0;
}
