#include<iostream>
#include<fstream>
#include"headers.h"
using namespace std;

#define header_bon 16
#define bon_unic 6000
int bonuri(long long val_bon[])
{
        int size_bon=0,i,junk,flag;
        char junky;
        ifstream g("../../input/bonuri.csv");

        g.seekg(header_bon,g.beg);
        long long x;
        while(g>>x)
        {
                flag =1;
                g>>junky>>junk;
                for(i=0;i<size_bon&&flag;i++)
                        if(x==val_bon[i])
                                flag=0;
                if(!flag)
                        continue;
                val_bon[size_bon]=x;
                size_bon++;
        }
        g.close();
        return size_bon-1;
}
//separare id-uri bonuri si unicizarea lor
void csv_to_unique_list()
{
        //transformare din bonuri.csv in lista unica de bonuri
        int size_bonuri;
        ofstream h("bonuri_unice.txt");
        long long val_bon[bon_unic];
        size_bonuri=bonuri(val_bon);
        for(int i=0;i<=size_bonuri;i++)
                h<<val_bon[i]<<"\n";
        h.close();
}


//aplicare functie hash pe valorile bonului
#define NUMAR_BITI 13

//transformare din lista unica de bonuri in valori hash
void unique_list_to_hash_map()
{
        unsigned long long x;
        ifstream g("bonuri_unice.txt");
        ofstream h("harta_hash.txt");
        while(g>>x)
                h<<has(x,NUMAR_BITI)<<"\n";

        g.close();
        h.close();
}
void count_colision()
{
        ifstream g("harta_hash.txt");
        int contor=0,coliziuni=0,valori[bon_unic],i;
        while(g>>valori[contor])
        {
                for(i=0;i<contor;i++)
                        if(valori[contor]==valori[i])
                        {
                                coliziuni++;
                                break;
                        }
                contor++;
        }
        cout<<coliziuni<<"\n";
        return;

}
int main()
{
        csv_to_unique_list();
        unique_list_to_hash_map();
        count_colision();
}
