#include <iostream>
#include "../../src/generic_data_structures/hash_map.h"
#include "../../src/hash_functions.h"

#define SIZE_H_MAP 1<<NUMAR_BITI //valoare statica
#define NUMAR_BITI 4 //valoare configurare

int main()
{
        HashMap<unsigned long long,int> X(10,func_x64);
        typename HashMap<unsigned long long,int>::Iterator it;
	//adaugare
        for(unsigned int i=0;i<=20;i++)
        {
                X.add(i,i+1);
        }
	//iterare + afisare
        for(it=X.begin(); it.isValid() ; ++it)
                std::cout<< it.getValue()<<" ";

	//scoatere valuare in functie de cheie
        try
        {
                int valu=X.getValue(16);
                std::cout<<"\nvalu = "<<valu;
        }
        catch(int exc)
        {
                std::cout<<"Error "<<exc<<". Key not found.\n";
        }
        return 0;
}
