#include "../../src/Clase/Palet.h"
#include <iostream>

using namespace std;

int main() {
	unsigned int x = 1;

	Palet a("wrw", 1, 2, 3);
	Palet c;
	//Test for assignment operator overloading
	c = a;
	//Test for copy constructor:
	Palet b = a;
	a.print();
	b.print();
	//Test for setslot
	a.setslot(x);
	a.print();
	return 0;
}