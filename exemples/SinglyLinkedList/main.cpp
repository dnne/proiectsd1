#include "../../src/generic_data_structures/singly_linked_list.h"
#include <iostream>

using namespace std;

int main()
{
	SinglyLinkedList<int> list;

	list.addLast(5);
	list.addLast(6);

	cout<<"print"<<endl;
	cout<<list<<endl;

	cout<<"Parcurgere si inmultire cu 2(Hell yea)"<<endl;
	for(typename SinglyLinkedList<int>::Iterator it=list.begin(); it != list.end() ; it++)
	{
		it.getValue()*=2;
		cout<<it.getValue()<<',';
	}
	cout<<endl;

	cout<<"print"<<endl;
	cout<<list<<endl;

	cout<<"removeFirst"<<endl;
	cout<<list.removeFirst()<<endl;
	cout<<list.removeFirst()<<endl;

	cout<<"print"<<endl;
	cout<<list<<endl;

	try{
		cout<<list.removeLast();
	}
	catch(int exc)
	{
		cout<<"Error "<<exc<<endl;
	}
	return 0;
}
